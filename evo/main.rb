require './genoma.rb'
require './os.rb'

ProfilesName = 'profiles.dat'

def flagParser(f,l)
	/[\S].*/.match(/[^#{f}].*/.match(/[\S].*/.match(/[\s]#{f}[\s]*[\S]+/.match(' '+l).to_s).to_s).to_s).to_s
end

def s_is_a? str
	(str =~ /\[[^,\]]+(,[^,\]]+)*\]$/) == 0
end

#def s_to_a str
#	a = []
#	str ||= ''
#	while str != '' do
#		str = /[^,\[][^\]]*/.match(str).to_s
#		a[a.length] = /[^,\]]*/.match(str).to_s
#		str = /[,$].*/.match(str).to_s
#	end
#	a
#end

#def a_to_s a
#	a.length > 1 ? " #{a[0]},#{a_to_s(a[1..a.length-1])}" : " #{a[0]} "
#end

def filtroProfiles?(l)
	p = flagParser('-p',l); n = flagParser('-n',l); s = flagParser('-s',l); w = flagParser('-w',l)
	p!='' && (n=='' || n==Cmd[:yo][0]) && (s=='' || s==Cmd[:yo][1]) && (w=='' || w==Cmd[:yo][2])
end

ProfilePrefix,Lista = lambda do
	r = ['.']
	File.open("#{ProfilesName}",'a')do end
	File.open("#{ProfilesName}",'r')do|f|while l=f.gets do if l[0..0]!='#' then
		if filtroProfiles?(l) then
			r[0] = flagParser('-p',l)
			tmp = flagParser('-l',l)
			if !(s_is_a? tmp) then r[1] = [] else
				r[1] = /[^\[][^\]]*/.match(tmp).to_s.split(',')
			end
			break
		end
	end end end
	r
end.call
ConfPath = ProfilePrefix+'Conf'

def confName n
	"#{ConfPath}/configuracion#{n}.conf"
end

def confParser fileName = "#{ConfPath}/configuracion.conf"
	## Valores por defecto
	configuracion = { 'fileName.freno.emergencia' => 'freno.emergencia', 'generaciones.path' => 'generaciones',
			'historial.name' => 'historial.hist', 'avances.name' => 'avances.hist', 'avancesI.name' => 'avan_iter.hist',
			'avances.iteraciones' => 'progreso.hist', 'regeneracion.ext' => 'reg',
			'genomas.path' => '../cubomas', 'genomas.ext' => 'cbm', 'gnuplot.ext' => 'gnuplot',
			'generaciones.def' => '1', 'iteraciones.def' => '10', 'simulador.flags' => '',
			'generacion.base' => '0', 'base.current' => 'cuboma1',
			'mutagenos.TPUGM' => '0.01', 'mutagenos.MMPA' => '0.01', 'mutagenos.genesValidos' => '0 1 2 3' }

	File.open(fileName,'a')do end	## Evita error: 'el fichero no existe'
	File.open(fileName,'r')do|f|
		while l = f.gets do if l[0..0] != "\n" && l[0..0]!='#' then	## Las lineas que empiecen por '#' son ignoradas (comentarios), también las líneas vacías
			a = l.split
			configuracion[a[0]] = a[1..a.length-1].join(' ')
		end end
	end
	configuracion
end

def actualizarConf(fileName,generacionBase,baseActual)
	a = []; i = 0; flags = [false,false]
	File.open(fileName,'r')do|f|while l = f.gets do
		case
		when (l =~ /generacion\.base/) == 0 then a[i] = "generacion.base #{generacionBase}"; flags[0] = true
		when (l =~ /base\.current/) == 0 then a[i] = "base.current #{baseActual}"; flags[1] = true
		when true then a[i] = l
		end
		i += 1
	end end
	if !flags[0] then a[i] = "generacion.base #{generacionBase}"; i+=1 end
	if !flags[1] then a[i] = "base.current #{baseActual}" end
	File.open(fileName,'w')do|f| a.each do|l| f.puts l end end
	puts "Se ha actualizado el fichero #{fileName}:"
	a.each do|l| puts l end
end

def quitarL(l)
	l = ' ' + l
	p = l =~ /[\s]-l[\s]*[\S]+/
	l[1..p] + l[p+/[\s]-l[\s]*[\S]+/.match(l).to_s.length..l.length]
end

def actualizarProfiles(nombre)
	a = []; i = 0; flag = true
	File.open(ProfilesName,'r')do|f|while l=f.gets do
		if flag && l[0..0]!='#' && filtroProfiles?(l) then
			lista = flagParser('-l',l)
			case
			when lista == '' then a[i] = l.chomp + " -l [#{nombre}]"
			when s_is_a?(lista) then
				listaA = /[^\[][^\]]*/.match(lista).to_s.split(',')
				listaA[listaA.length] = nombre
				a[i] = quitarL(l).chomp.chomp(' ') + " -l [#{listaA.join(',')}]"
			else a[i] = quitarL(l).chomp.chomp(' ') + " -l [#{nombre}]"
			end
			flag = false
		else
			a[i] = l
		end
		i+=1
	end end
	File.open(ProfilesName,'w')do|f| a.each do|l| f.puts l end end
	puts "Se ha actualizado el fichero #{ProfilesName}:"
	a.each do|l| puts l end
end

def setFreno fileName = 'freno.emergencia'
	File.open(fileName,'w') do end	## `touch #{fileName}`
#	puts "Aviso: Acaba de crearse el fichero \"#{fileName}\" en blanco,"
#	puts "\tsi se borra la ejecución fallará en el próximo cambio de generación,"
#	puts "\tdeteniendo el programa sin perdida de datos."
end

def enBucle(generaciones,iteraciones,g,i)
	ret = true
	if generaciones then ret = ret && generaciones > g end
	if iteraciones then ret = ret && iteraciones > i end
	ret
end

def simulacion(fileGenoma,flags)
##	puts cad = `#{Cmd[:cd]} .. ; #{Cmd[:optirun]} #{Cmd[:muelles]} #{fileGenoma} #{flags}`
	puts cad = `#{Cmd[:cd]} ../../../../bin ; #{Cmd[:muelles]} ../blocks/randomize/muelles/cubomas/#{fileGenoma} #{flags}`
	r = $?.exitstatus
	if r == 0 then d = /[\d]+\.[\d]+/.match(/distancia recorrida: [\d]+\.[\d]+/.match(cad).to_s).to_s.to_f else d = 0 end
	[r,d]
end

## params = {:confFileName, :mutagenos, :generaciones, :iteraciones, :base, :generacionBase, :flags}
def avanzar params = {}
	params = {:confFileName => 'configuracion.conf'}.merge params
	configuracion = confParser params[:confFileName]
	## Valores por defecto para parametros, sacados de confFileName
	if configuracion['genomas.ext'] == 'cbm' then
		params[:mutagenos] ||= [configuracion['mutagenos.TPUGM'].to_f, configuracion['mutagenos.genesValidos'].split]
	else params[:mutagenos] ||= [configuracion['mutagenos.MMPA'].to_f, configuracion['mutagenos.genesValidos'].split] end
	params[:generaciones] ||= configuracion['generaciones.def'].to_i
	params[:iteraciones] ||= configuracion['iteraciones.def'].to_i
	params[:base] ||= configuracion['base.current']
	params[:generacionBase] ||= configuracion['generacion.base'].to_i
	params[:flags] ||= configuracion['simulador.flags']
	setFreno configuracion['fileName.freno.emergencia']
	configuracion['generaciones.path'] += "/#{ProfilePrefix}/#{params[:base]}"
	puts "\n\tavanzar #{params[:base]}, #{params[:generaciones]} generaciones ó #{params[:iteraciones]} iteraciones"
## Evaluacion base
	puts "Empezando evaluación base."
  ## carga
	base = Genoma.new "#{configuracion['genomas.path']}/#{ProfilePrefix}/#{params[:base]}.#{configuracion['genomas.ext']}"
	base.setSim
  ## simulación
	r = simulacion("#{ProfilePrefix}/#{params[:base]}.#{configuracion['genomas.ext']}.m",params[:flags])
	if r[0] != 0 then puts 'Error en simulación base.'; exit else despBase = r[1] end
  ## evaluación
	generacion = 0; iteracion = 0
	while enBucle(params[:generaciones],params[:iteraciones],generacion,iteracion) do
		unless File.exist? configuracion['fileName.freno.emergencia'] then break end
		puts "\n\tEmpezando iteración: #{iteracion} < #{params[:iteraciones]}, generación nº #{generacion+params[:generacionBase]}, #{generacion} < #{params[:generaciones]}"
		puts "Tiempo = #{Time.now}"
	## mutación
		mutacion = Genoma.new base.fileName
		mutacion.mutar! params[:mutagenos]
		mutacion.setSim
	## simulación
		r = simulacion("#{ProfilePrefix}/#{params[:base]}.#{configuracion['genomas.ext']}.m",params[:flags])
		if r[0] != 0 then puts 'Error en simulación.'; break else desp = r[1] end
	## evaluación
		puts "Depuración: despBase = #{despBase}, desp = #{desp}"
		if desp > despBase then
			puts 'Encontrado modelo mejorado, este sera guardado como base, y el base se movera a:'
			puts "\t" + tmp = "#{configuracion['generaciones.path']}/#{params[:base]}.#{configuracion['genomas.ext']}.v_#{generacion+params[:generacionBase]}"
			File.open("#{configuracion['generaciones.path']}/#{configuracion['avances.name']}",'a')do|f|
				f.puts "generación #{generacion+params[:generacionBase]}, desplazamiento #{despBase}, mutación #{params[:mutagenos][0]}."
			end
			base.guardar tmp
			base = mutacion
			base.guardar base.fileName
			despBase = desp
			generacion += 1
		else
			puts 'La nueva iteracion no supone una mejora, se descarta.'
		end
		iteracion += 1
		sleep 1
	end
	if iteracion > 0 then File.open("#{configuracion['generaciones.path']}/#{configuracion['historial.name']}",'a')do|f|
			f.print "Se acaban de realizar #{iteracion} iteraciones, "
			f.print "con un factor de mutación #{params[:mutagenos][0]} por uno, "
			f.print "en las que se ha avanzado #{generacion} generaciones, "
			f.puts "quedando el genoma en la generación nº #{generacion+params[:generacionBase]}."
		end
		actualizarConf(params[:confFileName],generacion+params[:generacionBase],params[:base])
	end
end

## params = {:n tandas, :g generaciones, :i iteraciones, :f flags, :b break, :l lista}
def multi params = {}
	params[:n] ||= 1
	params[:g] ||= 300
	params[:i] ||= 1000
	params[:f] ||= '-b'
	params[:b] ||= 'freno.emergencia'
	params[:l] ||= Lista
	File.open(params[:b],'w')do end
	params[:n].times do|i|
		puts "ITERACIÓN: #{i} de #{params[:n]}, [#{params[:l].join(',')}]"
		params[:l].each do |l|
			avanzar :confFileName => confName(l), :flags => params[:f], :iteraciones=> params[:i], :generaciones=> params[:g]
			unless File.exist? params[:b] then break end
		end
		unless File.exist? params[:b] then break end
	end
end

def resumir conf
	configuracion = confParser conf
	path = "#{configuracion['generaciones.path']}/#{ProfilePrefix}/#{configuracion['base.current']}"
	File.open("#{path}/#{configuracion['historial.name']}",'r')do|f|
		i = 0; g = 0; g_ = 0
		while l = f.gets do
			i += /[\d]+/.match(/[\d]+[\s]*iteraciones/.match(l).to_s).to_s.to_i
			g += /[\d]+/.match(/[\d]+[\s]*generaciones/.match(l).to_s).to_s.to_i
			gt = /[\d]+/.match(/generación[\s]*nº[\s]*[\d]+/.match(l).to_s).to_s.to_i
			if gt < g_ then
				puts "# El archivo #{path}/#{configuracion['historial.name']} parece corrupto en la línea:\n#\t#{l}"
				puts "# ya que el valor de generación anterior #{g_} > #{gt}."
			end
			g_ = gt
		end
		print "Al cuboma #{configuracion['base.current']}, se le han hecho un total de #{i} iteraciones, "
		print "en las que ha avanzado #{g} generaciones, llegando hasta la generación #{g_}, "
		puts "es decir la actual generación será registrada como #{configuracion['generacion.base']}."
	end
end

def resumir_todos lista = Lista
	lista.each do|c| resumir confName(c) end
end

def regenerar_avances conf
	configuracion = confParser conf
	path = "#{configuracion['generaciones.path']}/#{ProfilePrefix}/#{configuracion['base.current']}"
	File.open("#{path}/#{configuracion['avances.name']}",'w')do end
	configuracion['generacion.base'].to_i.times do|i|
		l = "#{configuracion['base.current']}.#{configuracion['genomas.ext']}.v_#{i}"
		m = "#{ProfilePrefix}/#{configuracion['base.current']}.#{configuracion['regeneracion.ext']}.#{configuracion['genomas.ext']}"
		n = "#{configuracion['genomas.path']}/#{m}"
		`#{Cmd[:cp]} #{path}/#{l} #{n}`
		genoma = Genoma.new n
		genoma.setSim
		r = simulacion("#{m}.m",'-b')
		if r[0] != 0 then puts 'Error en simulación.'; exit else d = r[1] end
		File.open("#{path}/#{configuracion['avances.name']}",'a')do|f|
			f.puts "Generación #{i}, desplazamiento #{d}, mutación desconocida"
		end
	end
end

def regenerarA_todos lista = Lista
	lista.each do|c| regenerar_avances confName(c) end
end

def exportar_avances conf
	configuracion = confParser conf
	path = "#{configuracion['generaciones.path']}/#{ProfilePrefix}/#{configuracion['base.current']}"
	File.open("#{path}/#{configuracion['avances.name']}.#{configuracion['gnuplot.ext']}",'w')do|f2|
		File.open("#{path}/#{configuracion['avances.name']}",'r')do|f|while l=f.gets do
			f2.puts "#{/[\d]+/.match(l).to_s} #{/[\d]+\.[\d]+/.match(l).to_s}"
		end end
	end
end

def exportarA_todos lista = Lista
	lista.each do|c| exportar_avances confName(c) end
end

def exportar_historial conf
	configuracion = confParser conf
	path = "#{configuracion['generaciones.path']}/#{ProfilePrefix}/#{configuracion['base.current']}"
	i = 0.0
	File.open("#{path}/#{configuracion['historial.name']}.#{configuracion['gnuplot.ext']}",'w')do|f2|
		f2.puts '0 0'
		File.open("#{path}/#{configuracion['historial.name']}",'r')do|f|while l=f.gets do
			i += /[\d]+/.match(/[\d]+[\s]*iteraciones/.match(l).to_s).to_s.to_i.to_f
			g = /[\d]+/.match(/[\d]+[\s]*generaciones/.match(l).to_s).to_s.to_i
			g_ = /[\d]+/.match(/generación[\s]*nº[\s]*[\d]+/.match(l).to_s).to_s.to_i
			if g != 0 then
				f2.puts "#{g_} #{(i)/g_}"
			end
		end end
	end
end

def exportarH_todos lista = Lista
	lista.each do|c| exportar_historial confName(c) end
end

def exportarA_iter conf
	configuracion = confParser conf
	path = "#{configuracion['generaciones.path']}/#{ProfilePrefix}/#{configuracion['base.current']}"
	i = 0; salida = false
	File.open("#{path}/#{configuracion['avancesI.name']}.#{configuracion['gnuplot.ext']}",'w')do|f2|
		File.open("#{path}/#{configuracion['avances.name']}",'r')do|f3|
			a = /[\d]+\.[\d]+/.match(f3.gets).to_s.to_f; f2.puts "#{i} #{a}"
			File.open("#{path}/#{configuracion['historial.name']}",'r')do|f|while (l=f.gets) && !salida do
				i += /[\d]+/.match(/[\d]+[\s]*iteraciones/.match(l).to_s).to_s.to_i
				g_ = /[\d]+/.match(/generación[\s]*nº[\s]*[\d]+/.match(l).to_s).to_s.to_i
				g = 0
				while g < g_ && !salida do
					if la = f3.gets then
						g = /[\d]+/.match(la).to_s.to_i
						a = /[\d]+\.[\d]+/.match(la).to_s.to_f
					else salida = true end
				end
				if !salida then f2.puts "#{i} #{a}" end
			end end
		end
	end

end

def exportarAI_todos lista = Lista
	lista.each do|c| exportarA_iter confName(c) end
end

def meta_gnu lista = Lista	## Linux
	coma = ''
	File.open("#{ConfPath}/avances.plt",'w')do|f|
	File.open("#{ConfPath}/historial.plt",'w')do|f2|
	File.open("#{ConfPath}/avan_iter.plt",'w')do|f3|
		f.print "#!/usr/bin/gnuplot\n\nplot "
		f2.print "#!/usr/bin/gnuplot\n\nplot "
		f3.print "#!/usr/bin/gnuplot\n\nplot "
		lista.each do|c|
			conf = confParser confName(c)
			f.print "#{coma}\"./#{conf['generaciones.path']}/#{ProfilePrefix}/#{conf['base.current']}/#{conf['avances.name']}.#{conf['gnuplot.ext']}\"w l"
			f2.print "#{coma}\"./#{conf['generaciones.path']}/#{ProfilePrefix}/#{conf['base.current']}/#{conf['historial.name']}.#{conf['gnuplot.ext']}\"w l"
			f3.print "#{coma}\"./#{conf['generaciones.path']}/#{ProfilePrefix}/#{conf['base.current']}/#{conf['avancesI.name']}.#{conf['gnuplot.ext']}\"w l"
			coma = ','
		end
		f.puts "\npause -1 \"Pulsa INTRO para continuar\""
		f2.puts "\npause -1 \"Pulsa INTRO para continuar\""
		f3.puts "\npause -1 \"Pulsa INTRO para continuar\""
	end
	end
	end
	`chmod +x #{ConfPath}/avances.plt`
	`chmod +x #{ConfPath}/historial.plt`
	`chmod +x #{ConfPath}/avan_iter.plt`
end

def meta_resumir
	dumy = "require 'main'\nresumir_todos\n"
	`echo "#{dumy}" | #{Cmd[:ruby]}`.split("\n").each do|l|if l[0..0] != '#' then
		i = /[\d]+/.match(/[\d]+[\s]*iteraciones/.match(l).to_s).to_s.to_i
		g = /[\d]+/.match(/[\d]+[\s]*generaciones/.match(l).to_s).to_s.to_i
		cad = /[^,]+/.match(l).to_s
		cad[0..0] = 'E'
		puts "#{cad} ha tardado #{i/g} iteraciones por generación en promedio durante su evolución."
	end end
	nil
end

def crearGenoma(nombre,genoma,generacionBase,tipo)
	conf = "#{ConfPath}/configuracion#{nombre}.conf"
	gen = "../cubomas/#{ProfilePrefix}/#{tipo}#{nombre}.#{(tipo==:cuboma)?'cbm':'fqm'}"
	generaciones = "generaciones/#{ProfilePrefix}/#{tipo}#{nombre}"
	if File.exist?(conf) || File.exist?(gen) || File.exist?(generaciones) then
		puts "Ya existe alguno de los siguientes ficheros: #{conf}, #{gen}, #{generaciones}. Prueba con otro nombre."
	else
		File.open(conf,'w')do|f|
			if tipo == :frequoma then
				f.puts "genomas.ext fqm"
			end
			f.puts "base.current #{tipo}#{nombre}"
			f.puts "generacion.base #{generacionBase}"
		end
		puts "Se ha creado el fichero #{conf} con la configuración del nuevo #{tipo}."
		File.open(gen,'w')do|f|
			f.puts genoma
		end
		puts "Se ha creado el fichero #{gen} que contiene el nuevo #{tipo}."
		`#{Cmd[:mkdir]} #{generaciones}`
		puts "Se ha creado el directorio #{generaciones}, para contener el registro generacional del nuevo #{tipo}."
		actualizarProfiles(nombre)
	end
end
