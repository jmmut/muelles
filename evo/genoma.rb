class Genoma

	attr_reader :fileName

	def generar
		f = []
		@dimensiones.length.times do|d|
			f[d] = []
			@dimensiones[d].times do|i|
				v = 0
				arg = 2*Math::PI*i*(1.0/@dimensiones[d])
				@armonicos[d].length.times do|j| v+= @armonicos[d][j][0]*Math.cos(arg*j) + @armonicos[d][j][1]*Math.sin(arg*j) end
				f[d][i] = v
			end
		end
		@dimensiones[0].times do|i|	## Solo funciona con 3 dimensiones
			@dimensiones[1].times do|j|
				@dimensiones[2].times do|k|
					@datos[i*@dimensiones[1]*@dimensiones[2] + j*@dimensiones[2] + k] = 
						@genesValidos[ (f[0][i] + f[1][j] + f[2][k])% @genesValidos.length ]
				end
			end
		end
	end

	def initialize fileName
		@fileName = fileName
		cad = ''
		File.open(fileName,'r')do|f|
			tmp = f.gets.split
			@dimensiones = []
			tmp.length.times do|i|
				@dimensiones[i] = tmp[i].to_i
			end
			while l = f.gets do cad += l end
		end
		cad = cad.split
		@datos = []
		case fileName
		when /\.cbm(\.v_[0-9]+)?/ then
			@tipo = :cuboma
			cad.length.times do|i|
				@datos[i] = cad[i].to_i
			end
		when /\.fqm(\.v_[0-9]+)?/ then
			@tipo = :frequoma
			@genesValidos = cad[0].split(',')
			j = 1
			@armonicos = []
			@dimensiones.length.times do|d|
				@armonicos[d] = []
				(@dimensiones[d]/2+1).times do|i|
					@armonicos[d][i] = [cad[j].to_f,cad[j+1].to_f]
					j+=2
				end
			end
			self.generar
		else
			@tipo = :desconocido
			puts 'Intento de carga de Genoma con extensión de archivo desconocida.'
		end
	end

	## [TantoPorUnoDeGenesMutantes, GenesValidos] ejem: [0.01, [0,1,2,3]]
	## [MutaciónMáximaPorArmónico, GenesValidos] ejem: [0.01, [0,1,2,3]]
	def mutar! mutagenos
		case @tipo
		when :cuboma then
			@datos.length.times do |i|
				if rand < mutagenos[0] then @datos[i] = mutagenos[1][rand mutagenos[1].length].to_i end
			end
		when :frequoma then
			@armonicos.length.times do|d|
				@armonicos[d].length.times do|i|
					@armonicos[d][i][0] += mutagenos[0]*2*(0.5-rand)
					@armonicos[d][i][1] += mutagenos[0]*2*(0.5-rand)
				end
			end
			@genesValidos = mutagenos[1]
			self.generar
		when :desconocido then
			puts 'Intento de mutación de Genoma con @tipo = :desconocido.'
		end
	end

	def guardar fileName	## Solo funciona con cubomas de 3 dimensiones
		File.open(fileName,'w')do|f|
			f.puts @dimensiones.join(' ')
			case @tipo
			when :cuboma then
				@dimensiones[0].times do |i|	##
					f.puts ''
					@dimensiones[1].times do |j|
						d = i*@dimensiones[1]*@dimensiones[2] + j*@dimensiones[2]
						f.puts @datos[d..d+@dimensiones[2]-1].join(' ')
					end
				end
			when :frequoma then
				f.puts @genesValidos.join(',')
				@armonicos.length.times do|d|
					f.puts ''
					@armonicos[d].length.times do|i|
						f.puts @armonicos[d][i].join(' ')
					end
				end
			when :desconocido then
				puts 'Intento de guardar un Genoma con @tipo = :desconocido.'
			end
		end
	end

	def setSim
		case @tipo
		when :cuboma then self.guardar "#{@fileName}.m"
		when :frequoma then @tipo = :cuboma; self.guardar "#{@fileName}.m"; @tipo = :frequoma
		when :desconocido then puts 'Intento de llamar a setSim para un Genoma con @tipo = :desconocido.'
		end
	end

end
