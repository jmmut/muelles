#!/usr/bin/env ruby

require './main.rb'

case
when ARGV.length < 1 || (ARGV[0] =~ /-+h(elp)?$/) == 0 then
	puts "Uso1: #{$0} <confFileName> [<generaciones> [<iteraciones> [-h/b/i]]]"
	puts "Uso2: #{$0} -multi [<lista> [<generaciones> [<iteraciones> [-h/b/i]]]]"
	puts "Uso3: #{$0} -gnuplot [<lista>]"
	puts "Uso4: #{$0} -crearcuboma <nombre> [<ficheroOrigen> [<generacionBase>]]"
	puts "Uso5: #{$0} -crearfrequoma <nombre> [<ficheroOrigen> [<generacionBase>]]"
when (ARGV[0] =~ /-+c(rear)?(c(uboma)?|f(requoma)?)$/) == 0 then
	case ARGV[0]
	when /c(uboma)?$/ then
		tipo = :cuboma
		cad = '10 '*2+"10\n"+("\n"+('1 '*9+"1\n")*10)*10
	when /f(requoma)?$/ then
		tipo = :frequoma
		cad = '10 '*2+"10\n0,1,2,3\n"+("\n"+"0 0\n"*6)*3
	end
	case ARGV.length
	when 1 then puts "Se necesita un nombre para el nuevo #{tipo}."; exit
	when 3 then cad = '';File.open(ARGV[2],'r')do|f|while l=f.gets do cad+=l end end
	end
	crearGenoma(ARGV[1],cad,ARGV[3]?ARGV[3]:0,tipo)
when (ARGV[0] =~ /-+g(nu)?p(lot)?$/) == 0 then
	case
	when ARGV.length < 2 then 
		exportarA_todos
		exportarH_todos
		exportarAI_todos
		meta_gnu
	when !(s_is_a? ARGV[1]) then
		puts 'El parametro <lista> debe encajar con la siguiente expresión regular /\[[^,\]]+(,[^,\]]+)*\]$/'
		puts "ejms: [1], [1,2,3], [a,b,c], [#{Lista.join(',')}]."
		puts 'NOTA: no puede contener espacios.'
		exit
	else
		lista = /[^\[][^\]]*/.match(ARGV[1]).to_s.split(',')
		exportarA_todos lista
		exportarH_todos lista
		exportarAI_todos lista
		meta_gnu lista
	end
	`./#{ConfPath}/avances.plt`
	`./#{ConfPath}/historial.plt`
	`./#{ConfPath}/avan_iter.plt`
when (ARGV[0] =~ /-+m(ulti)?$/) == 0 then
	case
	when ARGV.length < 2 then multi :g=>0,:i=>0,:f=>''
	when !(s_is_a? ARGV[1]) then
		puts 'El parametro <lista> debe encajar con la siguiente expresión regular /\[[^,\]]+(,[^,\]]+)*\]$/'
		puts "ejms: [1], [1,2,3], [a,b,c], [#{Lista.join(',')}]."
		puts 'NOTA: no puede contener espacios.'
	else
		a = /[^\[][^\]]*/.match(ARGV[1]).to_s.split(',')
		ARGV[4] ||= ''
		multi :l => a, :g => ARGV[2].to_i, :i => ARGV[3].to_i, :f => ARGV[4]
	end
else
	ARGV[3] ||= ''
	avanzar :confFileName => ARGV[0], :generaciones => ARGV[1].to_i, :iteraciones => ARGV[2].to_i, :flags => ARGV[3]
end
