require 'rbconfig'

def os?
	@os ||= case RbConfig::CONFIG['host_os']
	when /mswin|msys|mingw|cygwin|bccwin|wince|emc/
		:windows
	when /darwin|mac os/
		:macosx
	when /linux/
		:linux
	when /solaris|bsd/
		:unix
	else :unknown
	end
end

Cmd = case os?
when :linux
        {:rm=>'rm', :cp=>'cp', :yo=>`uname -n;uname -s;whoami`.split("\n")}
when :windows
        {:rm=>'erase', :cp=>'copy', :yo=>`whoami`.chomp.split("\\").insert(1,'Windows')}
else
        puts 'Sistema operativo no soportado.'
        exit
end     # Comunes
Cmd[:cd] ||= 'cd'
Cmd[:ruby] ||= 'ruby'
##Cmd[:muelles] ||= './muelles'  # En windows podría ser 'muelles', pero así también funciona
Cmd[:muelles] ||= './randomize_muelles_src_main'  # En windows podría ser 'muelles', pero así también funciona
Cmd[:mkdir] ||= 'mkdir'

Cmd[:optirun] ||= '' 
#Cmd[:optirun] ||= case (`optirun --version`; $?.exitstatus)
#when 0 then 'optirun'
#else ''
#end
