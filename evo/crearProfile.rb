#!/usr/bin/env ruby

require './os.rb'

puts 'Elige el prefijo para el nuevo perfil (exp: .jos):'
p = gets.chomp

n = Cmd[:yo][0]
puts "El nombre de tu máquina es #{n}, si deseas usarlo para el perfil pulsa intro, si deseas usar otro escribelo, si desas dejarlo en blanco escribe 'nil'"
g = gets.chomp
case
when g == 'nil' then n = nil
when g != '' then n = g
end

s = Cmd[:yo][1]
puts "El nombre de tu SO es #{s}, si deseas usarlo para el perfil pulsa intro, si deseas usar otro escribelo, si deseas dejarlo en blanco escribe 'nil'"
g = gets.chomp
case
when g == 'nil' then s = nil
when g != '' then s = g
end

w = Cmd[:yo][2]
puts "El nombre de tu usuario es #{w}, si deseas usarlo para el perfil pulsa intro, si deseasusar otro escribelo, si deseas dejarlo en blanco escribe 'nil'"
g = gets.chomp
case
when g == 'nil' then w = nil
when g != '' then w = g
end

File.open('profiles.dat','a')do|f|
	f.print "-p #{p}"
	if n then f.print " -n #{n}"end
	if s then f.print " -s #{s}"end
	if w then f.print " -w #{w}"end
	f.puts
end
puts "Se ha añadido el nuevo perfil al archivo ./profiles.dat, es posible que prefiles preexistentes más generales puedan estar ocultandolo."

`#{Cmd[:mkdir]} #{p}Conf`
puts "Se ha creado el directorio ./#{p}Conf/, coloca en el los archivos de configuración que desees."
puts "Los que tengas de antes probablemente den problemas con la nueva organización de directorios, consejo: copiate los mios y cambia los números de generación a mano"

`#{Cmd[:mkdir]} ../cubomas/#{p}`
puts "Se ha creado el directorio ../cubomas/#{p}/, coloca en el los archivos de cubomas que desees."

`#{Cmd[:mkdir]} generaciones/#{p}`
puts "Se ha creado el directorio ./generaciones/#{p}/, coloca en el las carpetas de generaciones que desees."
