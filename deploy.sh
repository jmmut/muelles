#!/bin/bash
# (re)crea release y copia src, shaders y install.sh
rm -rf release
mkdir -p release/{src,lib,shaders}
cp -rL src/* release/src
cp shaders/* release/shaders
cp -p install.sh release/

# (re)crea el tar con un directorio auxiliar
cp -r release release-$(date +%Y-%m-%d)
rm -f release.tar.gz
tar -pzcvf release.tar.gz release-$(date +%Y-%m-%d)
rm -rf release-$(date +%Y-%m-%d)

