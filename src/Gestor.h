#ifndef _GESTOR_H_
#define _GESTOR_H_

#include <randomize/muelles/src/threads/Pool.h>
#include "vgm/Ventana.h"
#include "Memoria.h"

enum class Command {
    QUIT = 1,
    ECHO,
    ADD,
    KILL,
    GET,
    PAUSE,
    FREEZE,
    RESUME,
    VERBOSITY
};


class Gestor: public Ventana, public Memoria {
public:
    Gestor(int width = 640, int height = 480);
    virtual void initGL() override;
    virtual void onStep(float) override;
    virtual void onDisplay() override;
    virtual void onKeyPressed(SDL_Event &e) override;
    virtual void onPressed(const Pulsado &p) override;
    virtual void onMouseMotion(SDL_Event &e) override;
    virtual void onMouseButton(SDL_Event &e) override;
    virtual void onEvent(SDL_Event &e) override;

    static void printHelp();
    void setLogLevel(string level);
    void setup(const string &cbmPath);
    void setControlEvent(Uint32 controlEvent);
    void setThreads(int threads);
private:
    void drawAxes();

    int controlEvent;
    Uint32 simulationThreads = 4;
//    Pool pool;
    double time;
    float displayPeriod;

    void addCubome(const string &cbmPath);
};

class GestorSimulation: public Gestor {
public:
    GestorSimulation(int width = 640, int height = 480);
    void onStep(float);
    void setupSimulation(const string &cbmPath, const string &logPath);
    bool resultWrited();

private:

    bool writed;
    float distance;
    float finishTime;

};


class SimulationThread : public Pool::Thread {
public:
    SimulationThread(Pool &pool) : Thread(pool) {}
    void run() {
        SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "  2     starting run\n");
        Uint32 chunk = this->getChunkID();
        vector<SistemaCubos> * cubes = (vector<SistemaCubos> *) this->getData();
        SistemaCubos *cube = &(*cubes)[chunk];
        cube->avanzar(0.005);
        SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "   3    ending run\n");
    }
};

#endif
