
#define LOG_FILE_STYLE ""

#include <getopt.h>
#include <thread>
#include <string>
#include "Gestor.h"
#include "SistemaMuelles.h"
#include "utils/SignalHandler.h"

using std::string;
using std::cout;
using std::cin;
using std::endl;

void printLaunchHelp();
void worldCommand(int width, int height, string cbmFile, int fps, string logLevel);

string options = "hbi:pl:wv";
static struct option long_options[] = {
        {"help", 1, 0, 'h'},
        {"no-display", 1, 0, 'b'},	// blind
        {"iterations-per-second", 1, 0, 'i'},
        {"play", 1, 0, 'p'},
        {"log", 1, 0, 'l'},
        {"world", 1, 0, 'w'},
        {"verbose", 1, 0, 'v'},
        {0, 0, 0, 0},
};


int main(int argc, char **argv)
{
    int c = 0;
    int fps = 200;
    int width = 1300;
    int height = 700;
    std::string cbmFile, logFile;
    bool blindAsked = false;
    bool playAsked = false;
    bool logAsked = false;
    bool worldAsked = false;
    string logLevel = "info";
    const char *defaultCubomaPath = "cubomas/test.cbm";

    SigsegvPrinter::activate(cout);

    while (c != -1) {	// process options
        c = getopt_long(argc, argv, options.c_str(), long_options, NULL);
        switch (c) {
            case 'h':
                printLaunchHelp();
                break;
            case 'b':
                blindAsked = true;
                break;
            case 'i':
                fps = atoi(optarg);
                break;
            case 'p':
                playAsked = true;
                break;
            case 'l':
                logAsked = true;
                logFile = atoi(optarg);
                break;
            case 'w':
                worldAsked = true;
                break;
            case 'v':
                logLevel = "debug";
                break;
        }
    }

    if (playAsked && (logAsked || blindAsked)) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "inconsistent options provided.\n");
        return 1;	// error
    }

    if (optind < argc) {	// process files
        cbmFile = argv[optind++];
        if (optind < argc) {
            SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION, "extra parameters at command-line:\n");
            while (optind < argc) {
                SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION, "\t%s\n", argv[optind++]);
            }
        }
    }

    if (!blindAsked && !playAsked && !worldAsked && cbmFile.empty()) {
        playAsked = true;   // if no mode is specified, assume play
    }

    if (cbmFile.empty()) {	// default play
        cbmFile = defaultCubomaPath;
    }

    if (playAsked) {	// play
        Gestor gestor(width, height);
        gestor.setLogLevel(logLevel);
        gestor.setup(cbmFile);
        gestor.setFps(fps);
        gestor.mainLoop();
        return 1;
    } else if (blindAsked) {	// blind simulation
        SistemaCubos sc;
        int state;
        float dist = 0;
//		cbmFile.insert(0, "randomize/muelles/cubomas/");
        if (!logFile.empty()) {
            SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "logFile = %s\n", logFile.c_str());	// DEPURACION
            logFile.insert(0, "logs/");
        }

        sc.setupSimulation(cbmFile, logFile);
        sc.setAcelGlobal({0, -10, 0});
        while ((state = sc.runSimulation(10, 0.005, dist)) == 0) {
            cout << dist << "\r";
        }
    } else if (worldAsked) {
        worldCommand(width, height, cbmFile, fps, logLevel);
    } else {	// simulation
        GestorSimulation gestor(width, height);
        gestor.setLogLevel(logLevel);
        gestor.setupSimulation(cbmFile, logFile);
        gestor.setFps(fps);
        gestor.mainLoop();
    }


    return 0;
}

void worldCommand(int width, int height, string cbmFile, int fps, string logLevel) {
    Uint32 myEventType = SDL_RegisterEvents(1);

    if (myEventType != ((Uint32)-1)) {
        std::thread simulation([=]() {
            SigsegvPrinter::activate(cout);
            Gestor gestor(width, height);
            gestor.setLogLevel(logLevel);
            gestor.setup(cbmFile);
            gestor.setFps(fps);
            gestor.setControlEvent(myEventType);
            gestor.mainLoop();
            SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "exiting simulator thread\n");
        });

        string packageId, target, command, line(""), cbmPath, cbmId, verbosity;
        bool stop = false;
        SDL_LogInfo(SDL_LOG_CATEGORY_APPLICATION, "starting simulator shell\n");
        while (cin >> command, !cin.eof() && !stop) {
            if (command == "echo") {
                getline(cin, line);
                cout << packageId << " " << target << " " << command << line << endl;
            } else if (command == "quit" || command == "exit") {
                stop = true;
            } else if (command == "add") {
                cin >> cbmId;
                cin >> cbmPath;
                SDL_Event event;
                SDL_zero(event);
                event.type = myEventType;
                event.user.code = (int)Command::ADD;
                event.user.data1 = (void *) cbmPath.c_str();
                event.user.data2 = (void *) cbmId.c_str();
                SDL_PushEvent(&event);
            } else if (command == "get") {
//                cin >> packageId;
                cout << "unsupported operation" << endl;
            } else if (command == "kill") {
                cout << "unsupported operation" << endl;
            } else if (command == "pause") {
                SDL_Event event;
                SDL_zero(event);
                event.type = myEventType;
                event.user.code = (int)Command::PAUSE;
                SDL_PushEvent(&event);
            } else if (command == "resume") {
                SDL_Event event;
                SDL_zero(event);
                event.type = myEventType;
                event.user.code = (int)Command::RESUME;
                SDL_PushEvent(&event);
            } else if (command == "verbosity") {
                cin >> verbosity;
                SDL_Event event;
                SDL_zero(event);
                event.type = myEventType;
                event.user.code = (int)Command::VERBOSITY;
                event.user.data1 = (void *) verbosity.c_str();
                SDL_PushEvent(&event);
            } else if (packageId == "help" || target == "help" || command == "help") {
                cout << "** simulator shell help ** Commands are of the form:\n\t<command> [args]\n\n"
                << "command: one of {help, quit, echo, pause, freeze, resume, add, get, kill}\n"
                << " * quit: synonyms: exit, CTRL+D. The only safe way to quit, because of the thread handling.\n"
                << " * echo <message>: echo message.\n"
                << " * pause: pause simulation, so it stops computing but still draws.\n"
                << " * freeze: pause simulation and drawing.\n"
                << " * resume: resume simulation and drawing.\n"
                << " * add <cubomeName> <cubomePath>: loads cubome from file.\n"
                << " * UNIMPLEMENTED get <property> <cubomeName> <packageId>: returns a property of a cubome.\n"
                << " * UNIMPLEMENTED kill <cubomeName>: stops a cubome simulation.\n"
                << " * verbosity <verbosity>: changes log level [debug|info|error].\n";
            }
        }

        SDL_Event event;
        SDL_zero(event);
        event.type = myEventType;
        event.user.code = (int)Command::QUIT;
//        event.user.data1 = 0;
//        event.user.data2 = 0;
        SDL_PushEvent(&event);  // The event is copied into the queue, and the caller may dispose of the memory pointed to after SDL_PushEvent() returns
        SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "exiting main thread.");
        simulation.join();
    } else {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "could not create custom event\n");
    }

    SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "exiting main thread. this should not be displayed because the SDL is already Quit()ed\n");
}


void printLaunchHelp() {
    struct option *o;

    cout << "** simulator CLI help ** this program simulates a spring system.\nIt has five modes: play with a default model:\n\t$ ./muelles\n\n"
    << "Play with a choosed cubome:\n\t$ ./muelles <cubome.cbm> -p\n\n"
    << "Test the velocity of a model:\n\t$ ./muelles <cubome.cbm>\n\n"
    << "Blind test:\n\t$ ./muelles <cubome.cbm> -b\n\n"
    << "World simulation:\n\t$ ./muelles -w\n\n";
    cout << "Usage:\n\t$ ./muelles [<cbm>] [<options>]\nIf you don't give anything, you will play with a default cubome."
    << " If you provide a cubome, you will simulate that. You can write the result of the simulation with -l<log>.\n\n";
    cout << "Invocation options are:\n";
    for (o = long_options; o->name; ++o) {
        cout << "-" << (char)o->val << " --" << o->name << endl;
    }
}

