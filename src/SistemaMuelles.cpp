#include "SistemaMuelles.h"


constexpr float SistemaMuelles::k;	// I couldn't workaroud this, it seems k is not allocated without this
constexpr float SistemaMuelles::r;
constexpr float SistemaMuelles::m;
const CubeEdges SistemaMuelles::cubeEdges;

SistemaMuelles::SistemaMuelles() {
    actual = 0;
    t = 0;
}

SistemaMuelles::~SistemaMuelles() {
}

void SistemaMuelles::setCuerda(int numNodes, float longitud) {
    int i;
    float longPorMuelle = longitud/(numNodes-1);

    g.clear();
    for (i = 0; i < numNodes; i++)
    {
        //g.addNode({{Vector3D(i*longPorMuelle, 1, 1),Vector3D(0, 0, 0)},{Vector3D(0, 0, 0), Vector3D(0, 0, 0)}});
        g.addNode(Node(Vector3D(i*longPorMuelle, 1, 1)));
    }
    for (i = 0; i < numNodes-1; i++)
    {
        g.addEdge(i, i+1, {longPorMuelle});
    }
}

void SistemaMuelles::setCubo(float lado, CubeType tipo) {
    float diag2 = sqrtf(lado*lado + lado*lado);
    float diag3 = sqrtf(lado*lado + lado*lado + lado*lado);
    g.clear();
    g.addNode(Node(Vector3D(0, 0, 0)));
    g.addNode(Node(Vector3D(lado, 0, 0)));
    g.addNode(Node(Vector3D(0, lado, 0)));
    g.addNode(Node(Vector3D(0, 0, lado)));
    g.addNode(Node(Vector3D(lado, lado, 0)));
    g.addNode(Node(Vector3D(0, lado, lado)));
    g.addNode(Node(Vector3D(lado, 0, lado)));
    g.addNode(Node(Vector3D(lado, lado, lado)));
    g.addEdge(0, 1, {lado, lado, tipo});	// basic cube
    g.addEdge(0, 2, {lado, lado, tipo});
    g.addEdge(0, 3, {lado, lado, tipo});
    g.addEdge(4, 1, {lado, lado, tipo});
    g.addEdge(4, 2, {lado, lado, tipo});
    g.addEdge(4, 7, {lado, lado, tipo});
    g.addEdge(5, 2, {lado, lado, tipo});
    g.addEdge(5, 3, {lado, lado, tipo});
    g.addEdge(5, 7, {lado, lado, tipo});
    g.addEdge(6, 1, {lado, lado, tipo});
    g.addEdge(6, 3, {lado, lado, tipo});
    g.addEdge(6, 7, {lado, lado, tipo});
    g.addEdge(0, 7, {diag3, diag3, tipo});	// opposite vertices
    g.addEdge(1, 5, {diag3, diag3, tipo});
    g.addEdge(6, 2, {diag3, diag3, tipo});
    g.addEdge(3, 4, {diag3, diag3, tipo});
    g.addEdge(0, 4, {diag2, diag2, tipo});	// face edges
    g.addEdge(0, 5, {diag2, diag2, tipo});
    g.addEdge(0, 6, {diag2, diag2, tipo});
    g.addEdge(6, 4, {diag2, diag2, tipo});
    g.addEdge(4, 5, {diag2, diag2, tipo});
    g.addEdge(5, 6, {diag2, diag2, tipo});
    g.addEdge(7, 1, {diag2, diag2, tipo});
    g.addEdge(7, 2, {diag2, diag2, tipo});
    g.addEdge(7, 3, {diag2, diag2, tipo});
    g.addEdge(1, 2, {diag2, diag2, tipo});
    g.addEdge(2, 3, {diag2, diag2, tipo});
    g.addEdge(3, 1, {diag2, diag2, tipo});
}
void SistemaMuelles::setCubo(float lado, unsigned int names[8], CubeType tipo) {
    float diag2 = sqrtf(lado*lado + lado*lado);
    float diag3 = sqrtf(lado*lado + lado*lado + lado*lado);
    /*
       for (auto p: getCubeEdges()) {
       switch (get<2>(p)) {
       case 1:
       g.addEdge(names[get<0>(p)], names[get<1>(p)], {lado, lado});	// basic cube
       break;
       case 2:
       g.addEdge(names[get<0>(p)], names[get<1>(p)], {diag2, diag2});	// basic cube
       break;
       case 3:
       g.addEdge(names[get<0>(p)], names[get<1>(p)], {diag3, diag3});	// basic cube
       break;
       }
       }
       */
    for (unsigned int i = 0; i < cubeEdges.length; i++) {
        switch (cubeEdges.cubeEdge[i].type) {
            case 1:
                g.addEdge(names[cubeEdges.cubeEdge[i].src], names[cubeEdges.cubeEdge[i].dst], {lado, lado, tipo});	// basic cube
                break;
            case 2:
                g.addEdge(names[cubeEdges.cubeEdge[i].src], names[cubeEdges.cubeEdge[i].dst], {diag2, diag2, tipo});	// basic cube
                break;
            case 3:
                g.addEdge(names[cubeEdges.cubeEdge[i].src], names[cubeEdges.cubeEdge[i].dst], {diag3, diag3, tipo});	// basic cube
                break;
        }
    }
}

void SistemaMuelles::setTetra(float lado) {
    g.clear();
    g.addNode(Node(Vector3D(0, 0, 0)));
    g.addNode(Node(Vector3D(lado, 0, 0)));
    g.addNode(Node(Vector3D(0, lado, 0)));
    g.addNode(Node(Vector3D(0, 0, lado)));
    g.addEdge(0, 1, {lado, lado});	// basic cube
    g.addEdge(0, 2, {lado, lado});
    g.addEdge(0, 3, {lado, lado});
    g.addEdge(1, 2, {lado, lado});
    g.addEdge(2, 3, {lado, lado});
    g.addEdge(3, 1, {lado, lado});
}

/**
 * @param nodo must list the nodes clokwise (or counter-clockwise), not rows
 * and columns.
 */
void SistemaMuelles::appendCubo(Vector3D dir, unsigned int cuantos,  unsigned int nodo[4]) {
    Node n[4];
    unsigned int i, j;
    unsigned int newIds[4];
    float lado = dir.Modulo()*0.75l;
    //float diag2 = sqrtf(lado*lado + lado*lado);
    //float diag3 = sqrtf(lado*lado + lado*lado + lado*lado);
    float diag3 = lado*3;

    for (j = 0; j < cuantos; j++) {
        for (i = 0; i < 4; i++) {	// copy neighbours nodes
            newIds[i] = g.getNodesCount();
            g.getNode(nodo[i], n[i]);
            n[i].pos[actual] += dir;
            g.addNode(n[i]);
        }

        for (i = 0; i < 4; i++) {
            g.addEdge(nodo[i], newIds[i], {lado});	// basic cube
            g.addEdge(newIds[i], newIds[(i+1)%4], {lado});	// basic cube
            //g.addEdge(newIds[i], nodo[(i+1)%4], {diag2});	// face edges
            //g.addEdge(newIds[i], nodo[(i+3)%4], {diag2});	// face edges
            g.addEdge(newIds[i], nodo[(i+2)%4], {diag3});	// opposite vertices
        }
        for (i = 0; i < 4; i++) {
            nodo[i] = newIds[i];
        }
    }
    //g.addEdge(newIds[0], newIds[2], {diag2});	// outer face
    //g.addEdge(newIds[1], newIds[3], {diag2});

    //cout << "a partir de :" << nodo[0] << ", "  << nodo[1] << ", " << nodo[2] << ", " << nodo[3] << "; "
    //<< newIds[0] << ", "  << newIds[1] << ", " << newIds[2] << ", " << newIds[3] << endl;
}


/**
 * @param nodo must be left pair, center pair, right pair.
 */
void SistemaMuelles::appendCubo2(unsigned int nodo[6]) {
    Node n[6];
    int i;
    unsigned int newIds[2];

    for (i = 0; i < 2; i++) {	// copy neighbours nodes
        newIds[i] = g.getNodesCount();
        g.getNode(nodo[i], n[i]);
        g.getNode(nodo[i+2], n[i+2]);
        g.getNode(nodo[i+4], n[i+4]);
        n[i].pos[actual] += n[i].pos[actual] + n[i+4].pos[actual] - n[i+2].pos[actual];	// fill the paralelogram
        g.addNode(n[i]);
    }
    float lado = (n[0].pos[actual] - n[2].pos[actual]).Modulo();
    //float diag2 = sqrtf(lado*lado + lado*lado);
    //float diag3 = sqrtf(lado*lado + lado*lado + lado*lado);
    float diag3 = lado*3;

    for (i = 0; i < 2; i++) {
        g.addEdge(nodo[i], newIds[i], {lado});	// basic cube
        g.addEdge(nodo[i+4], newIds[i], {lado});	// basic cube
        //g.addEdge(newIds[i], nodo[i+2], {diag2});	// face edges
        //g.addEdge(nodo[i], nodo[i+4], {diag2});	// face edges
    }
    g.addEdge(newIds[0], newIds[1], {lado});	// basic cube
    //g.addEdge(newIds[0], nodo[1], {diag2});	// face edges
    //g.addEdge(newIds[0], nodo[5], {diag2});	// face edges
    g.addEdge(newIds[0], nodo[3], {diag3});	// outer face
    //g.addEdge(newIds[1], nodo[0], {diag2});	// face edges
    //g.addEdge(newIds[1], nodo[4], {diag2});	// face edges
    g.addEdge(newIds[1], nodo[2], {diag3});	// outer face
    g.addEdge(nodo[1], nodo[4], {diag3});	// outer face
    g.addEdge(nodo[0], nodo[5], {diag3});	// outer face


    cout << "a partir de :" << nodo[0] << ", "  << nodo[1] << ", " << nodo[2] << ", " << nodo[3] << ", "
    << nodo[4] << ", "  << nodo[5] << "; " << newIds[0] << ", " << newIds[1] << endl;
}

/*
   void SistemaMuelles::appendCubo3(unsigned int nodo[7]) {
   Node n[4];
   int i;
   unsigned int newIds[4];
   float lado = dir.Modulo();
   float diag2 = sqrtf(lado*lado + lado*lado);
   float diag3 = sqrtf(lado*lado + lado*lado + lado*lado);

   for (i = 0; i < 4; i++) {	// copy neighbours nodes
   newIds[i] = g.getNodesCount();
   g.getNode(nodo[i], n[i]);
   n[i].pos[actual] += dir;
   g.addNode(n[i]);
   }

   for (i = 0; i < 4; i++) {
   g.addEdge(nodo[i], newIds[i], {lado});	// basic cube
   g.addEdge(newIds[i], newIds[(i+1)%4], {lado});	// basic cube
   g.addEdge(newIds[i], nodo[(i+1)%4], {diag2});	// face edges
   g.addEdge(newIds[i], nodo[(i+3)%4], {diag2});	// face edges
   g.addEdge(newIds[i], nodo[(i+2)%4], {diag3});	// opposite vertices
   }
   g.addEdge(newIds[0], newIds[2], {diag2});	// outer face
   g.addEdge(newIds[1], newIds[3], {diag2});

   cout << "a partir de :" << nodo[0] << ", "  << nodo[1] << ", " << nodo[2] << ", " << nodo[3] << "; "
   << newIds[0] << ", "  << newIds[1] << ", " << newIds[2] << ", " << newIds[3] << endl;
   }
   */

void SistemaMuelles::setAcelGlobal(Vector3D a)
{
    grav = a;
}


void SistemaMuelles::setTriangTensegrity(float lado) {
    int i;
    g.clear();
    g.addNode(Node(Vector3D(1, 1, 1)));
    g.addNode(Node(Vector3D(2, 1, 1)));
    g.addNode(Node(Vector3D(2, 1, 2)));
    g.addNode(Node(Vector3D(1, 2, 1)));
    g.addNode(Node(Vector3D(2, 2, 1)));
    g.addNode(Node(Vector3D(2, 2, 2)));
    for (i = 0; i < 3; i++)
    {
        g.addEdge(i, (i+1)%3, {lado});	// lower triangle
        g.addEdge(i+3, (i+1)%3+3, {lado});	// upper triangle
        g.addEdge(i, i+3, {lado});	// vertical links
        g.addEdge((i+1)%3, i+3, {2*lado});
    }
}

void SistemaMuelles::setTetraTensegrity(float lado) {
    g.clear();
    float diag = 0.25*lado;
    lado *= 2;
    g.addNode(Node(Vector3D(0, 0, 0)));
    g.addNode(Node(Vector3D(lado, 0, 0)));
    g.addNode(Node(Vector3D(0, lado, 0)));
    g.addNode(Node(Vector3D(0, 0, lado)));
    g.addNode(Node(Vector3D(0, 0, 0.5)));
    g.addEdge(0, 1, {lado, lado});	// basic envolture
    g.addEdge(0, 2, {lado, lado});
    g.addEdge(0, 3, {lado, lado});
    g.addEdge(1, 2, {lado, lado});
    g.addEdge(2, 3, {lado, lado});
    g.addEdge(3, 1, {lado, lado});
    g.addEdge(4, 0, {diag, diag});	// inner tensors
    g.addEdge(4, 1, {diag, diag});
    g.addEdge(4, 2, {diag, diag});
    g.addEdge(4, 3, {diag, diag});
}


void SistemaMuelles::setCuboTensegrity(float lado) {
    //float diag2 = sqrtf(lado*lado + lado*lado);
    //float diag3 = sqrtf(lado*lado + lado*lado + lado*lado);
    lado *= 0.75;
    float diag3 = lado*3;
    g.clear();
    g.addNode(Node(Vector3D(0, 0, 0)));
    g.addNode(Node(Vector3D(lado, 0, 0)));
    g.addNode(Node(Vector3D(0, lado, 0)));
    g.addNode(Node(Vector3D(0, 0, lado)));
    g.addNode(Node(Vector3D(lado, lado, 0)));
    g.addNode(Node(Vector3D(0, lado, lado)));
    g.addNode(Node(Vector3D(lado, 0, lado)));
    g.addNode(Node(Vector3D(lado, lado, lado)));
    g.addEdge(0, 1, {lado});	// basic cube
    g.addEdge(0, 2, {lado});
    g.addEdge(0, 3, {lado});
    g.addEdge(4, 1, {lado});
    g.addEdge(4, 2, {lado});
    g.addEdge(4, 7, {lado});
    g.addEdge(5, 2, {lado});
    g.addEdge(5, 3, {lado});
    g.addEdge(5, 7, {lado});
    g.addEdge(6, 1, {lado});
    g.addEdge(6, 3, {lado});
    g.addEdge(6, 7, {lado});
    g.addEdge(0, 7, {diag3});	// opposite vertices
    g.addEdge(1, 5, {diag3});
    g.addEdge(6, 2, {diag3});
    g.addEdge(3, 4, {diag3});
    //g.addEdge(0, 4, {diag2});	// face edges
    //g.addEdge(0, 5, {diag2});
    //g.addEdge(0, 6, {diag2});
    //g.addEdge(6, 4, {diag2});
    //g.addEdge(4, 5, {diag2});
    //g.addEdge(5, 6, {diag2});
    //g.addEdge(7, 1, {diag2});
    //g.addEdge(7, 2, {diag2});
    //g.addEdge(7, 3, {diag2});
    //g.addEdge(1, 2, {diag2});
    //g.addEdge(2, 3, {diag2});
    //g.addEdge(3, 1, {diag2});
}

void SistemaMuelles::setCuboTensegrity(float lado, unsigned int names[8]) {
    float diag3 = lado*3;
    float diag2 = lado*2;
    lado *= 0.75;
    g.addEdge(names[0], names[1], {lado, lado});	// basic cube
    g.addEdge(names[0], names[2], {lado, lado});
    g.addEdge(names[0], names[3], {lado, lado});
    g.addEdge(names[4], names[1], {lado, lado});
    g.addEdge(names[4], names[2], {lado, lado});
    g.addEdge(names[4], names[7], {lado, lado});
    g.addEdge(names[5], names[2], {lado, lado});
    g.addEdge(names[5], names[3], {lado, lado});
    g.addEdge(names[5], names[7], {lado, lado});
    g.addEdge(names[6], names[1], {lado, lado});
    g.addEdge(names[6], names[3], {lado, lado});
    g.addEdge(names[6], names[7], {lado, lado});
    g.addEdge(names[0], names[7], {diag3, diag3});	// opposite vertices
    g.addEdge(names[1], names[5], {diag3, diag3});
    g.addEdge(names[6], names[2], {diag3, diag3});
    g.addEdge(names[3], names[4], {diag3, diag3});
    g.addEdge(names[0], names[4], {diag2, diag2});	// face edges
    g.addEdge(names[0], names[5], {diag2, diag2});
    g.addEdge(names[0], names[6], {diag2, diag2});
    g.addEdge(names[6], names[4], {diag2, diag2});
    g.addEdge(names[4], names[5], {diag2, diag2});
    g.addEdge(names[5], names[6], {diag2, diag2});
    g.addEdge(names[7], names[1], {diag2, diag2});
    g.addEdge(names[7], names[2], {diag2, diag2});
    g.addEdge(names[7], names[3], {diag2, diag2});
    g.addEdge(names[1], names[2], {diag2, diag2});
    g.addEdge(names[2], names[3], {diag2, diag2});
    g.addEdge(names[3], names[1], {diag2, diag2});
}
/*
   void SistemaMuelles::setMalla3D(unsigned int nX,unsigned int nY,unsigned int nZ, float lado) {
   unsigned int idsZ = {3, 6, 7, 5};
   unsigned int idsY = {2, 5, 7, 4};
   unsigned int idsX = {1, 4, 7, 6};
   setCuboTensegrity(lado);
   appendCubo(Vector3D(1, 0, 0), nX, idsX);
   appendCubo(Vector3D(0, 1, 0), 1, idsY);



   for (iz = 0; iz < nZ; iz++) {
   for (iy = 0; iy < nY; iy++) {
   for (ix = 0; ix < nX; ix++) {

   }
   }
   }
   }
// */

/** return a vector of 3-tuples with the list of edges that form a cube. the
 * first element is the source node, the second is the destiny node, and the
 * third is the kind of node (1=regular edge, 2=face edge, 3=edge linking
 * opposite vertices).
 */
CubeEdges SistemaMuelles::getCubeEdges() {
    //vector<tuple<unsigned int, unsigned int, int>> SistemaMuelles::getCubeEdges() {
    /*
       vector<tuple<unsigned int, unsigned int, int>> v;
       v.push_back(make_tuple(0, 1, 1));	// basic cube
       v.push_back(make_tuple(0, 2, 1));
       v.push_back(make_tuple(0, 3, 1));
       v.push_back(make_tuple(4, 1, 1));
       v.push_back(make_tuple(4, 2, 1));
       v.push_back(make_tuple(4, 7, 1));
       v.push_back(make_tuple(5, 2, 1));
       v.push_back(make_tuple(5, 3, 1));
       v.push_back(make_tuple(5, 7, 1));
       v.push_back(make_tuple(6, 1, 1));
       v.push_back(make_tuple(6, 3, 1));
       v.push_back(make_tuple(6, 7, 1));
       v.push_back(make_tuple(0, 7, 3));	// opposite vertices
       v.push_back(make_tuple(1, 5, 3));
       v.push_back(make_tuple(6, 2, 3));
       v.push_back(make_tuple(3, 4, 3));
       v.push_back(make_tuple(0, 4, 2));	// face edges
       v.push_back(make_tuple(0, 5, 2));
       v.push_back(make_tuple(0, 6, 2));
       v.push_back(make_tuple(6, 4, 2));
       v.push_back(make_tuple(4, 5, 2));
       v.push_back(make_tuple(5, 6, 2));
       v.push_back(make_tuple(7, 1, 2));
       v.push_back(make_tuple(7, 2, 2));
       v.push_back(make_tuple(7, 3, 2));
       v.push_back(make_tuple(1, 2, 2));
       v.push_back(make_tuple(2, 3, 2));
       v.push_back(make_tuple(3, 1, 2));
       return v;
       */
    return cubeEdges;
}

///////////////////////////////////////

unsigned int SistemaMuelles::getNodesCount() {
    return g.getNodesCount();
}

void SistemaMuelles::avanzar(float dt) {
    unsigned int i;
    Vector3D f;
    Vector3D a;
    Node nodo;
    Node iterNode;
    t += dt;

    //for (i = 0; i < g.getNodesCount(); i++) {
    //changeLong(i, dt);
    //}
    changeLong(dt);
    for (i = 0; i < g.getNodesCount(); i++) {
        g.getNode(i, iterNode);
        nodo.fuerza = getSumaFuerzaVecinos(i);
        nodo.vecinos = r*getSumaVelocidadVecinos(i);
        f = nodo.fuerza + nodo.vecinos;
        //f = getSumaFuerzaVecinos(i) + r*getSumaVelocidadVecinos(i);
        a = f/m + grav;
        nodo.pos[!actual] = a*dt*dt/2 + iterNode.vel[actual]*dt + iterNode.pos[actual];	// metodo itegracion euler
        nodo.vel[!actual] = iterNode.vel[actual];
        if (nodo.pos[!actual].y <= 0) {
            nodo.pos[!actual].y = 0;
            //nodo.vel[!actual] = Vector3D(0, -0.5*nodo.vel[!actual].y, 0);
            nodo.vel[!actual].y *= -0.75;
            nodo.vel[!actual] *= 0.75;
        }

        nodo.vel[!actual] += a*dt;
        nodo.pos[actual] = iterNode.pos[actual];
        nodo.vel[actual] = iterNode.vel[actual];

        g.replaceNode(i, nodo);	// hemos creado otro nodo con las posiciones siguentes actualizadas
    }
    actual = !actual;
}

Vector3D SistemaMuelles::getSumaVelocidadVecinos(unsigned int nodoId) {
    ///*		// slow version
    Vector3D v(0, 0, 0);
    Node nodoVecino, nodo;

    g.getNode(nodoId, nodo);
    g.forEachNeighbour(nodoId, [&](unsigned int vecinoId, Muelle m) {
        g.getNode(vecinoId, nodoVecino);
        v += nodoVecino.vel[actual] - nodo.vel[actual];
    });

    return v;
    // */
    /*
   Vector3D v(0, 0, 0);
   unsigned int vecinoId;
   Vector3D dist;
   Node nodoVecino, nodo;
   Neighbours *ng;

   g.getNode(nodoId, nodo);

   if (g.getNeighbours(nodoId, ng)) {
       for (const auto &e: *ng) {	// para cada edge vecino a nodoId
           vecinoId = e.first;
           g.getNode(vecinoId, nodoVecino);
           v += nodoVecino.vel[actual] - nodo.vel[actual];
       }
   }
   return v;
   // */
}
Vector3D SistemaMuelles::getSumaFuerzaVecinos(unsigned int nodoId) {
///*		// slow version

    Vector3D f(0, 0, 0);
    Vector3D dist;
    float modulo;
    Node nodoVecino, nodo;

    g.getNode(nodoId, nodo);
    g.forEachNeighbour(nodoId, [&](unsigned int vecinoId, Muelle &e) {
        g.getNode(vecinoId, nodoVecino);
        dist = nodoVecino.pos[actual] - nodo.pos[actual];
        modulo = dist.Modulo();
        if (modulo != 0) {
            f += k*dist*(1 - e.longitudActual/modulo);	//tension respecto a este vecino
        }
    });

    return f;
    // */
    /*

       Vector3D f(0, 0, 0);
       unsigned int vecinoId;
       Vector3D dist;
       float modulo;
       Node nodoVecino, nodo;
       Neighbours *ng;

       g.getNode(nodoId, nodo);

       if (g.getNeighbours(nodoId, ng)) {
       for (const auto &e: *ng) {	// para cada edge vecino a nodoId
       vecinoId = e.first;
       g.getNode(vecinoId, nodoVecino);
       dist = nodoVecino.pos[actual] - nodo.pos[actual];
       modulo = dist.Modulo();
       if (modulo != 0) {
       f += k*dist*(1 - e.second.longitudActual/modulo);	//tension respecto a este vecino
       }
       }

       }
       return f;
    // */
}

void SistemaMuelles::changeLong(float dt) {

}
/*
void SistemaMuelles::changeLong(unsigned int id, float dt) {
	Neighbours *ng;
	//Muelle m;
	float variationAmplitude = 0.2;
	float cachedSin = sin(4*t)*variationAmplitude;
	if (g.getNeighbours(id, ng)) {
		for (auto &e: *ng) {	//for each neighbour edge of the node "id"
			e.second.longitudActual = e.second.longitudReposo + e.second.longitudReposo*cachedSin;	// every edge set separately
		}
		*/
/*  // slow encapsulated version
    for (Edge e: *ng) {	//for each neighbour edge of the node "id"
    if (e.dst > id) {
    m = {e.val.longitudReposo, e.val.longitudReposo + e.val.longitudReposo*cachedSin};
    g.replaceEdge(id, e.dst, m);
//e.val.longitudActual = e.val.longitudReposo + e.val.longitudReposo*cachedSin;
}
}
*/
/*
	}

}
*/
void SistemaMuelles::anclar(unsigned int nodo) {
    Node n;
    g.getNode(nodo, n);
    n.pos[actual] = n.pos[!actual];	// en la no actual esta la pos anterior
    n.vel[actual] = n.vel[!actual];	// asi los cambios se pierden
    g.replaceNode(nodo, n);
}

void SistemaMuelles::setPos(unsigned int nodo, Vector3D posicion) {
    Node n;

    g.getNode(nodo, n);
    n.pos[actual] = posicion;
    g.replaceNode(nodo, n);
}

Vector3D SistemaMuelles::getPos(unsigned int nodo) {
    Node n;
    if (g.getNode(nodo, n)) {
        return n.pos[actual];
    }

    return Vector3D(0, 0, 0);
}

void SistemaMuelles::print(ostream &o) {
    g.printGraph(o);
}


void SistemaMuelles::getColor(CubeType type, float color[4]) {
    switch (type) {
        case EMPTY:
        case STATIC:
            color[0] = 0.0;
            color[1] = 0.0;
            color[2] = 1.0;
            color[3] = 0.2;
            break;
        case EXPAND_CONTRACT:
            color[0] = 1.0;
            color[1] = 0.0;
            color[2] = 0.0;
            color[3] = 0.2;
            break;
        case CONTRACT_EXPAND:
            color[0] = 0.0;
            color[1] = 1.0;
            color[2] = 0.0;
            color[3] = 0.2;
            break;
        default:
            color[0] = 1.0;
            color[1] = 1.0;
            color[2] = 0.0;
            color[3] = 0.2;
            break;
    }
}
void SistemaMuelles::draw() {
    float color[4];
    Node nodoVecino, nodo;
    unsigned int vecinoId;
    glBegin(GL_LINES);
    for (unsigned int i = 0; i < g.getNodesCount(); i++) {	// para cada nodo
        g.getNode(i, nodo);
        g.forEachNeighbour(i, [&](unsigned int dst, Muelle &m){
            vecinoId = dst;
            g.getNode(vecinoId, nodoVecino);
            getColor(m.tipo, color);
            glColor4fv(color);
            glVertex3fv(nodo.pos[actual].v);	// pintamos una linea
            //glVertex3fv(((nodoVecino.pos[actual]-nodo.pos[actual])/3 + nodo.pos[actual]).v);	//
            glVertex3fv(nodoVecino.pos[actual].v);
        });
    }
    glEnd();
}
/*
   void SistemaMuelles::draw() {
   unsigned int i;
   Neighbours *ng;
   Node nodo, vecino;

   glBegin(GL_LINES);
   for (i = 0; i < g.getNodesCount(); i++) {	// para cada nodo
   if (g.getNeighbours(i, ng)) {
   g.getNode(i, nodo);
//cout << "g.getNode(10000, nodo) = " << g.getNode(10000, nodo) << endl;	// DEPURACION
//cout << "nodo.pos = " << nodo.pos[0] << endl;	// DEPURACION
//cout << "g.getNodesCount() = " << g.getNodesCoun]t() << endl;	// DEPURACION
//cout << "g.nodes.capacity() = " << g.nodes.capacity() << endl;	// DEPURACION

glColor3f(1, 0, 0);
glVertex3fv(nodo.pos[actual].v);	// pintamos una linea
glVertex3fv((nodo.pos[actual] + nodo.fuerza).v);	//
glColor3f(0, 1, 0);
glVertex3fv(nodo.pos[actual].v);	// pintamos una linea
glVertex3fv((nodo.pos[actual] + vecino.vecinos).v);	//
glColor3f(1, 1, 0);
glVertex3fv(nodo.pos[actual].v);	// pintamos una linea
glVertex3fv((nodo.pos[actual] + grav*m).v);	//
for (const auto &e: *ng) {	// y para cada uno de sus vecinos
g.getNode(e.first, vecino);
//glColor3f(e.val.longitudReposo - e.val.longitudActual, e.val.longitudActual - e.val.longitudReposo, 1);
glColor3f(e.second.longitudActual - (nodo.pos[actual]-vecino.pos[actual]).Modulo(), (nodo.pos[actual]-vecino.pos[actual]).Modulo() - e.second.longitudActual, 1);
glVertex3fv(nodo.pos[actual].v);	// pintamos una linea
glVertex3fv(vecino.pos[actual].v);	//
}
}
}
glEnd();

}
*/
