#include "Pool.h"

Pool::Pool( Uint32 T, void *D): data( D), sem( SDL_CreateSemaphore( 0)), threads( T), threadsA( new Thread*[T]){}

Pool::~Pool(){
    for( Uint32 u=0; u < this->threads; u++){
        this->threadsA[u]->sendExitSignal();
        delete this->threadsA[u];
    }
    delete[] this->threadsA;
    SDL_DestroySemaphore( this->sem);
}

void Pool::sendExitSignal(){
    for( Uint32 u=0; u < this->threads; u++) this->threadsA[u]->sendExitSignal();
}

void Pool::sendDispatcherSignal(){
    SDL_SemPost( this->sem);
}

void Pool::setData( void *data){
    this->data = data;
}

void *Pool::getData(){
    return this->data;
}

void Pool::run( Uint32 chuncks){
    Uint32 u,v,w;
    for( u=0; u < chuncks && u < this->threads; u++){
        this->threadsA[u]->setChunkID(u);
        this->threadsA[u]->sendContinueSignal();
    }
    w = u-1;
    SDL_SemWait( this->sem);
    for(; u < chuncks; u++) for( v=0; v < this->threads; v++) if( this->threadsA[v]->isFree()){
                this->threadsA[v]->setChunkID(u);
                this->threadsA[v]->sendContinueSignal();
                SDL_SemWait( this->sem);
                v = this->threads;	// break;
            }
    for(; w > 0; w--) SDL_SemWait( this->sem);
}

//template< class Class>
//void Pool::createThreads(){
//	for( Uint32 u=0; u < this->threads; u++) this->threadsA[u] = new Class( *this);
//}

Pool::Thread::Thread( Pool &P): exit( false), free( true), chunkID( 0), sem( SDL_CreateSemaphore( 0)), pool( P){
//	this->thread = SDL_CreateThread( (int (*)(void*)) Pool::Thread::wrapper, nullptr, (void*) this);
//	SDL_DetachThread( this->thread);


//	SDL_DetachThread( SDL_CreateThread( (int (*)(void*)) Pool::Thread::wrapper, nullptr, (void*) this));    // TODO update biicode sdl to 2.0.2
    SDL_CreateThread( (int (*)(void*)) Pool::Thread::wrapper, nullptr, (void*) this);
}

Pool::Thread::~Thread(){
    SDL_DestroySemaphore( this->sem);
}

void *Pool::Thread::getData(){
    return this->pool.getData();
}

Uint32 Pool::Thread::getChunkID(){
    return this->chunkID;
}

void Pool::Thread::sendExitSignal(){
    this->exit = true;
    SDL_SemPost( this->sem);
}

void Pool::Thread::sendContinueSignal(){
    this->free = false;
    SDL_SemPost( this->sem);
}

bool Pool::Thread::isFree(){
    return this->free;
}

void Pool::Thread::setChunkID(Uint32 chunckID){
    this->chunkID = chunckID;
}

int Pool::Thread::wrapper( void *void_this){
    Pool::Thread *virtual_this = (Pool::Thread*) void_this;
    SDL_SemWait( virtual_this->sem);
    while( !( virtual_this->exit)){
        virtual_this->run();
        virtual_this->free = true;
        virtual_this->pool.sendDispatcherSignal();
        SDL_SemWait( virtual_this->sem);
    }
    return 0;
}

