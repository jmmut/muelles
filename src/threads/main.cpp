#include <iostream>
#include <SDL.h>
#include "Pool.h"

class MiThread: public Pool::Thread{
public:
    MiThread( Pool &pool):Thread(pool){}
    void run(){
        Uint32 u = this->getChunkID();
        int (&matriz)[10][10] = *((int (*)[10][10]) this->getData());
        matriz[u/10][u%10] += u;
    }
};

int main(int argc, char *argv[]){
    Uint32 u,v;
    int matriz[10][10];
    Pool piscina( 4, (void*) &matriz);

    for( u=0; u<10; u++) for( v=0; v<10; v++) matriz[u][v] = 0;

    SDL_Init(SDL_INIT_VIDEO);

    piscina.createThreads< MiThread>();

    piscina.run(100);

    for( u=0; u<10; u++){
        for( v=0; v<10; v++)
            std::cout << matriz[u][v] << ' ';
        std::cout << std::endl;
    }

    piscina.run( 100);

    for( u=0; u<10; u++){
        for( v=0; v<10; v++)
            std::cout << matriz[u][v] << ' ';
        std::cout << std::endl;
    }

    piscina.sendExitSignal();

    SDL_Quit();
    return 0;
}

