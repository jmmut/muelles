#ifndef Pool_H
#define Pool_H

#include <SDL.h>

class Pool{
public:
    class Thread{
        friend class Pool;
    private:
        bool exit, free;
        Uint32 chunkID;
        SDL_sem *sem;
//		SDL_Thread *thread;
        Pool &pool;
        void sendExitSignal();
        void sendContinueSignal();
        bool isFree();
        void setChunkID(Uint32 chunckID);
        static int wrapper( void *void_this);
    protected:
        Uint32 getChunkID();
        void *getData();
    public:
        Thread( Pool &P);
        ~Thread();
        virtual void run() = 0;
    };
private:
    void *data;
    SDL_sem *sem;
    Uint32 threads;
    Thread **threadsA;
    void sendDispatcherSignal();
public:
    Pool( Uint32 T, void *D = nullptr);
    ~Pool();
    void sendExitSignal();
    void setData( void *data);
    void *getData();
    void run( Uint32 chuncks);
    template< class Class>
    void createThreads(){
        for( Uint32 u=0; u < this->threads; u++) this->threadsA[u] = new Class( *this);
    }
};

#endif//Pool_H
