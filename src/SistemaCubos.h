#ifndef _SISTEMA_CUBOS_H
#define _SISTEMA_CUBOS_H

#include "SistemaMuelles.h"
#include <fstream>
#include <string>

class SistemaCubos: public SistemaMuelles {
public:
    SistemaCubos();
    ~SistemaCubos();
    void build(unsigned int x, unsigned int y, unsigned int z, float lado);
    void load(Cubo *cubos, unsigned int cuantos[3], float lado);
    void load(istream &in, unsigned int cuantos[3], float lado);
    void load(const string &cubomaPath);

    void setupSimulation(const string &cbmPath, const string &logPath);
    int runSimulation (float finishTime, float dt, float &distance);
    void writeSimulation (float distance);
    //void draw();
    //private:
    unsigned int hashCube(unsigned int x, unsigned int y, unsigned int z);
    unsigned int hashNode(unsigned int x, unsigned int y, unsigned int z);
    void unhashCube(unsigned int n, unsigned int &xCube, unsigned int &yCube, unsigned int &zCube);
    void unhashNode(unsigned int n, unsigned int &xNode, unsigned int &yNode, unsigned int &zNode);
    unsigned int nodeToCube(unsigned int node);
    void getColor(unsigned int cube, float color[4]);
    virtual void changeLong(float dt);
    //void changeLong(unsigned int cubeId, float dt);
private:
    unsigned int x, y, z, yx, zyx;	// sizes of the Cube matrix
    unsigned int xNodes, yNodes, zNodes, yxNodes;	// sizes of the Node matrix
    vector<Cubo> cubes;
    std::string cbmPath, logPath;
};
#endif // _SISTEMA_CUBOS_H
