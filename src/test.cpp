/**************************************************
 *
 * autor: Jose Miguel Mut
 *
 * fecha: 2013/12/19 20:31:59
 *
 * descripcion: timetest.cpp
 *		arriba defines funciones, y pones una llamada en el main real
 *		Utiliza asserts para parar el programa (y ver donde ha parado) 
 *			cuando algo no va como lo habias programado.
 *
 ***************************************************/

#include <iostream>
#include <chrono>
#include <tuple>
#include <stdio.h>
#include <cassert>
#include <randomize/common-libs/src/utils/SignalHandler.h>

using namespace std;
using namespace chrono;	// necessary to use directly seconds, nanoseconds, steady_clock, duration_cast

#include "utils/graph/Undirgravec.h"
#include "SistemaMuelles.h"
#include "SistemaCubos.h"
#include "utils/test/Test.h"

using namespace randomize::utils::test;

#define DEBUG(a) std::cout << #a << " = " << (a) << std::endl

static const char *const defaultCubomaPath = "randomize/muelles/cubomas/test.cbm";

int main2 (int argc, char **argv)	// undirgraph creation and clear
{
    Undirgravec<int, float> g;
    //float e;
    int ok = 0;

    ok += g.addNode(0);
    ok += g.addNode(1);
    ok += g.addNode(2);
    ok += g.addNode(3);
    ok += g.addNode(4);
    ok += g.addEdge(0, 4, 0.3);
    ok += g.addEdge(1, 4, 0.4);
    ok += g.addEdge(2, 4, 0.5);
    ok += g.addEdge(3, 4, 0.6);
    ok += g.addEdge(1, 3, 0.7);
    ok += g.addEdge(1, 2, 0.8);
    ok += g.addEdge(0, 1, 0.9);
    g.printGraph(cout);
    cout << "---------------" << endl;
    //Undirgraph<int, float>::Neighbours *ng;
//
    //if (g.getNeighbours(4, ng)) {
    //for(Undirgraph<int, float>::Edge e: *ng) {
    ////cout << e.src << "-" << e.dst;
    //cout << 4 << "-" << e.dst;
    //cout << ": " << e.val << endl;
    //}
    //}
    //g.getEdgeValue(4, 2, e);
//
    //assert(e == 0.5);
    //assert(g.getNodesCount() == 5);
    //assert(ok==0);
//
    g.clear();
//
    //assert(g.getNodesCount() == 0);

    return 0;
}

int sisCubes (int argc, char **argv) {	// SistemaCubos hash functions
    SistemaCubos sc;
    sc.build(7, 10, 6, 1);
    unsigned int x, y, z, id = sc.hashCube(2, 3, 5);
    assert(id == 7*10*6-1 - (6*7+4));
    sc.unhashCube(id, x, y, z);
    assert(x==2 && y==3 && z==5);

    id = sc.hashNode(2, 3, 5);
    assert(id == 8*11*7-1 - (11*8 + 7*8 + 5));
    sc.unhashNode(id, x, y, z);
    assert(x==2 && y==3 && z==5);
    return 0;
}

int refs (int argc, char **argv) {	// tests with std::vector references
    /*
    SistemaCubos sc;
    sc.build(2, 2, 2, 1);
    SistemaCubos::Neighbours *ng;
    SistemaCubos::Edge ed;
    sc.g.getEdgeValue(0, 1, ed.val);
    cout << "e.val.longitudActual = " << ed.val.longitudActual << endl;	// DEPURACION
    sc.g.getNeighbours(0, ng);
    for (SistemaCubos::Edge &e: *ng){
        cout << "e.val.longitudActual = " << e.val.longitudActual << endl;	// DEPURACION
        e.val.longitudActual = 234;
        cout << "&(e.val.longitudActual) = " << &(e.val.longitudActual) << endl;	// DEPURACION
    }
    sc.g.getNeighbours(0, ng);
    for (SistemaCubos::Edge &e: *ng){
        cout << "e.val.longitudActual = " << e.val.longitudActual << endl;	// DEPURACION
        cout << "&(e.val.longitudActual) = " << &(e.val.longitudActual) << endl;	// DEPURACION
    }
    sc.g.getEdgeValue(0, 1, ed.val);

    cout << "e.val.longitudActual = " << ed.val.longitudActual << endl;	// DEPURACION
    cout << "&(ed.val.longitudActual) = " << &(ed.val.longitudActual) << endl;	// DEPURACION

    sc.g.printGraph(cout);
    */
    return 0;
}

int performance (int argc, char **argv) {
    SistemaCubos sc;

    sc.load(defaultCubomaPath);
    sc.setAcelGlobal({0, -10, 0});

    //sc.g.forEachNeighbour($`unsigned int nodeId`, $`function<void (unsigned int, const SistemaMuelles::Muelle &)> func`);
    return 0;
}

int determinism (int argc, char ** argv) {
    SistemaCubos sc;
    float dist = 0;

    sc.setupSimulation(defaultCubomaPath, "test.log");
    sc.setAcelGlobal({0, -10, 0});
    while (sc.runSimulation(10, 0.005, dist) == 0) {
        cout << dist << "\r";
    }

    assert (fabsf(dist - 5.29967) < 0.003);
    return 0;
}
//////////////////////////////////////////////
/*
using namespace std;
class N {
public:
    vector<int> n;
    vector<pair<int, function<N(N&)>>> functions;  //< @param pair<arity, lambda>
//    vector<pair<int, function<int(int ...)>>> functions;  //< @param pair<arity, lambda>

    N() : n(), functions() {}
    N(int i) : n{i}, functions() {}
    N(initializer_list<int> i) : n(i) {}
    N(const N &n2) : n(n2.n), functions(n2.functions) {}
//    N operator()(N newN) {
//        return N(newN);
//    }
    N operator()(function<int(int)> f) {
        if (n.size() >= 1) {
            N n2(f(n[0]));
            n2.n.insert(n2.n.end(), ++n.begin(), n.end());
            return n2;  // maintaining the rest of elements
        }
        functions.push_back(pair<int, function<N(N&)>>(1, [](N &futureN) {
            return N();
        }));
        return N();
    }
    N operator()(function<int(int,int)> f) {
        if (n.size() >= 2) {
            N n2(f(n[0],n[1]));
            n2.n.insert(n2.n.end(), ++ ++n.begin(), n.end());
            return n2;
        }
//        if (n.size() == 1) {
//            return [f, this](int a) {
//                return f(a, n[0]);
//            };
//        }
        functions.push_back(pair<int,function>(2,f));
        return N();
    };

//        return this->operator()([](){
//            function<int(int)>
//        });
//    }
    N& operator=(N& n2) {
        n2.n = n;
        return n2;
    }
    N& operator>>(N& n2) {
        n2.n = n;
        return n2;
    }
    N operator,(N n2) {
        N n3;
        n3.n.insert(n3.n.end(), n.begin(), n.end());
        n3.n.insert(n3.n.end(), n2.n.begin(), n2.n.end());
        int i;
        for (i = 0; i < functions.size(); i++) {
            if (functions[i].first > n3.n.size()) {
                break;
            }
            (*this)(functions[i].second) >> n3;
        }
        int j;
        for (j = 0; j < n2.functions.size(); j++) {
            if (n2.functions[j].first > n3.n.size()) {
                break;
            }
            (*this)(n2.functions[j].second) >> n3;
        }
        n3.functions.insert(n3.functions.end(), functions.begin() +i, functions.end());
        n3.functions.insert(n3.functions.end(), n2.functions.begin() +j, n2.functions.end());
        return n3;
    }
    friend ostream& operator<<(ostream &o, N &n) {
        if (n.n.size() != 1) {
            o << "[";
        }
        if (n.n.size() >= 1) {
            o << n.n[0];
        }
        for (int i = 1; i < n.n.size(); i++) {
            o << ", " << n.n[i];
        }
        if (n.n.size() != 1) {
            o << "]";
        }
        if (n.functions.size() > 0) {
            o << "." << n.functions.size();
        }
        return o;
    }
};


*/

int test(int (*func)(int argc, char **argv), int argc, char **argv);
int main (int argc, char **argv){
    int ret;

    SigsegvPrinter::activate(cout);

/*
    function<int(int)> doblar = [](int n) {
        return 2*n;
    };

    function<int(int)> sumarUno = [](int n) {
        return n +1;
    };


    N a(5);
    N b(4);
    N c(6);
    N d(1);
    N e(1);
    N f(1);
    N g(1);
    N h(1);
    N i(1);
    N j(1);
    N k(1);
    N l(1);
    N uno(1);
    cout << "a = " << a << endl;
    cout << "b = " << b << endl;
    cout << "c = " << c << endl;
    (a(doblar)(sumarUno) >> b)(sumarUno)(sumarUno)(doblar) = c;
    cout << "a = " << a << endl;
    cout << "b = " << b << endl;
    cout << "c = " << c << endl << endl;

    function<int(int, int)> sumar = [](int a, int b) {
        return a + b;
    };
    function<int(int, int)> potencia = [](int a, int b) {
        return int(pow(a, b));
    };

    N(3) >> a;
    N(4) >> b;
    N(5) >> c;
    cout << "a = " << a << endl;
    cout << "b = " << b << endl;
    cout << "c = " << c << endl;
//    a(doblar)(c(sumar)) >> b; // deprecated
    (a, b)(potencia) >> c;
    a, b(sumarUno) >> d;
    (a, b(sumarUno)) >> e;
    (a, b(sumarUno))(potencia) >> f;
    a (potencia) >> g;
    (a, b, uno) (potencia) (sumar) >> h;
    ((a, b, uno) (potencia) >> i) (sumar) >> j;
    (((a, b, uno) (potencia) >> i) (sumar), i) (sumar) >> l;
    cout << "a = " << a << endl;
    cout << "b = " << b << endl;
    cout << "c = " << c << endl;
    cout << "d = " << d << endl;
    cout << "e = " << e << endl;
    cout << "f = " << f << endl;
    cout << "g = " << g << endl;
    cout << "h = " << h << endl;
    cout << "i = " << i << endl;
    cout << "j = " << j << endl;
    cout << "l = " << l << endl;

    cout << endl;

*/

    ret = time_test(main2, argc, argv);
    ret = time_test(sisCubes, argc, argv);
    ret = time_test(refs, argc, argv);
    ret = time_test(performance, argc, argv);
    ret = time_test(determinism, argc, argv);

    cout << "Todo ok" << endl;
    return ret;
}

