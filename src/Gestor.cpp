#include "Gestor.h"

Gestor::Gestor(int width, int height) : Ventana(width, height) {//}, pool(simulationThreads, &cubes) {
    setFps(80);
    initGL();
    //if (!initShaders())
    //exit(1);
    //LoadShaders("./shaders/mandelbrot.vs.glsl", "./shaders/mandelbrot.fs.glsl");

    t = 0;

    vldr.setOrientacion(Vector3D(1, 0.0, 0).Unit(), Vector3D(0, 1, 0));
    vldr.setPos(Vector3D(-10, 2, 5));

    drot = 1;
    dtrl = 0.1;
    dmv = 0.07;
    velocidad = 10;
    ancla = Vector3D(0, 0, 0);
    anclar = true;
    controlEvent = (Uint32)-1;
//    pool.createThreads<SimulationThread>();
    displayPeriod = 0.04;
    time = 0;

    //semaforoDisplay.Cerrar();
    //semaforoDisplay.Sumar();
    semaforoDisplay.abrir();
    //semaforoStep.Abrir();
    semaforoStep.cerrar();
}

GestorSimulation::GestorSimulation(int width, int height):Gestor(width, height){
    writed = false;
    distance = 0;
    finishTime = 10;
    velocidad = 1;
    semaforoStep.abrir();
    //semaforoDisplay.abrir();
    semaforoDisplay.cerrar();
    setFps(200);
}

void Gestor::setup(const string &cbmPath) {
    scosm = true;
    string cbm(cbmPath);
    //sm.setCuerda(50, 2);

    //sm.setCubo(1);
    //sm.setCuboTensegrity(1);
    //nCubos = 10;

    //sm.setTriangTensegrity(1);
    //sm.setCuboTensegrity(1);


    if (scosm) {
        /*
           unsigned int cuantos[3] = {10, 10, 10};
           int nCubos = cuantos[0]*cuantos[1]*cuantos[2];
           Cubo *cubos = new Cubo[nCubos];
           for (int i = 0; i < nCubos; i++) {
        //cubos[i].tipo = CubeType(rand()%3 + 1);
        //cubos[i].tipo = CubeType(i*3/nCubos + 1);
        //cubos[i].tipo = CubeType((i*3/nCubos + 2)%3+1);
        cubos[i].tipo = CubeType((i*3/nCubos + 2)%3+1);
        }
        sc.load(cubos, cuantos, 0.5);
        cout << "sc.getNodesCount() = " << sc.getNodesCount() << endl;	// DEPURACION
        delete[] cubos;
        */
        //sc.build(nCubos, nCubos, nCubos, 1);

//        sc.load(cbm);
//		sc.setAcelGlobal({0, -10, 0});
        addCubome(cbmPath);
    } else {
        sm.setTetraTensegrity(5);
        sm.setAcelGlobal({0, -10, 0});
    }

    //sm.setTetra(1);
}

void GestorSimulation::setupSimulation(const string &cbmPath, const string &logPath){
    scosm = true;
//	string cbm(cbmPath), log(logPath);
//	sc.setAcelGlobal({0, -10, 0});
//
//	cbm.insert(0, "cubomas/");
//	if (!log.empty()) {
//		log.insert(0, "logs/");
//	}
//	sc.setupSimulation(cbm, log);
    cubes.push_back(SistemaCubos());
    cubes.back().setupSimulation(cbmPath, logPath);
    cubes.back().setAcelGlobal({0, -10, 0});
}

bool GestorSimulation::resultWrited() {
    return writed;
}

/** A general OpenGL initialization function.
 * Sets all of the initial parameters.
 * We call this right after our OpenGL window is created.
 */
void Gestor::initGL()
{
    GLdouble aspect;
    int width = 640, height = 480;

    if (window == NULL)
        SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION
                , "There's no window, GL will use (640, 480) in the viewport, and you must create the sdl gl context ");
    else
    {
        SDL_GetWindowSize(window, &width, &height);
        context = SDL_GL_CreateContext(window);
        if (!context) {
            SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Unable to create OpenGL context: %s\n", SDL_GetError());
            SDL_Quit();
            exit(2);
        }
    }

    glViewport(0, 0, width, height);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);        // This Will Clear The Background Color To Black
    glClearDepth(1.0);                // Enables Clearing Of The Depth Buffer
    glDepthFunc(GL_LESS);                // The Type Of Depth Test To Do
    glEnable(GL_DEPTH_TEST);            // Enables Depth Testing
    glShadeModel(GL_SMOOTH);            // Enables Smooth Color Shading

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();                // Reset The Projection Matrix

    aspect = (GLdouble)width / height;

    perspectiveGL (45, aspect, 0.1, 100);

    //glOrtho(-1.0, 1.0, -1.0 / aspect, 1.0 / aspect, 0.0, 1.0);

    glMatrixMode(GL_MODELVIEW);
}


void Gestor::onStep(float dt) {
    t++;
    time += dt;
    if (time > displayPeriod) {
        time -= displayPeriod;
        semaforoDisplay.sumar();
    }
    if (scosm) {
//        SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, " 1      before run\n");

        for (SistemaCubos &cube : cubes) {
            cube.avanzar(0.005);
        }
//        pool.run(cubes.size());
//        SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "    4   after run\n");
//        for (SistemaCubos &cube : cubes) {
//            cube.avanzar(dt / velocidad);
//            if (anclar) {
//                cube.anclar(0);
//            }
//        }
    } else {
        sm.avanzar(dt/velocidad);
        if (anclar)
            sm.anclar(0);
    }
    //cout << "isPressed('g') = " << isPressed('g') << endl;	// DEPURACION
    //cout << "isPressed('G') = " << isPressed('G') << endl;	// DEPURACION
    //cout << "isPressed(SDL_SCANCODE_G) = " << isPressed(SDL_SCANCODE_G) << endl;	// DEPURACION
}

void GestorSimulation::onStep(float dt) {
    float dtFixed = 0.005;
    int state;
    if ((t&3) == 0) {
        semaforoDisplay.sumar();
    }
    state = cubes[0].runSimulation(finishTime, dtFixed/velocidad, distance);
    if (state >= 1) {	// simulation finished
        //semaforoStep.Cerrar();
        //semaforoDisplay.Abrir();
        //setFps(30);

        // o esto?
        writed = state == 2? false: true;	// if runSimulation could not write, returns 2.

        salir();
    }
    t++;
}


void Gestor::drawAxes()
{
    glPushMatrix();
    glBegin(GL_LINES);
    glColor3f(1, 0, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(100, 0, 0);

    glColor3f(0, 1, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 100, 0);

    glColor3f(0, 0, 1);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 0, 100);
    glEnd();

    glBegin(GL_TRIANGLES);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 2, 0);
    glVertex3f(0, 2, 1);
    glEnd();
    glPopMatrix();
}



void Gestor::onDisplay()
{
    // Clear The Screen And The Depth Buffer
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    //glEnable(GL_BLEND);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


    glLoadIdentity();                // Reset The View
    vldr.Look();
    //	glTranslatef( 0, 0, -5.0);

    drawAxes();
    glPushMatrix();

    //glTranslatef( 0, sin(t*0.05), -5.0);
    //glTranslatef( 0, 0, -5.0);
    //glRotatef(t, 0, 1, 0);

    glColor4f(0, 0.5, 1, 0.2);
    //glColor4f(0, 0.5, 1, 1.0);

    //glBegin( GL_TRIANGLES ); // Drawing Using Triangles
    //glVertex3f( 0.0f, 1.0f, 0.0f ); // Top
    //glVertex3f( -1.0f, -1.0f, 0.0f ); // Bottom Left
    //glVertex3f( 1.0f, -1.0f, 0.0f ); // Bottom Right
    //glEnd( ); // Finished Drawing The Triangle

    if (scosm) {
//        SDL_Log(SDL_LOG_CATEGORY_APPLICATION, "     5  before draw\n");
        for (SistemaCubos &cube : cubes) {
            cube.draw();
        }
//        SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "      6 after draw\n");
    } else {
        sm.draw();
    }
    glPopMatrix();

    SDL_GL_SwapWindow(window);
}

void Gestor::onPressed(const Pulsado &p)
{
    //cout << "p.sym = " << p.sym << endl;	// DEPURACION

    switch(p.sym)
    {
        case SDLK_u:
            //vldr.setPos(vldr.getPos() + Vector3D(0, dtrl, 0));
            vldr.rotatef(-drot, 0, 0, 1);
            break;
        case SDLK_o:
            //vldr.setPos(vldr.getPos() + Vector3D(0, -dtrl, 0));
            vldr.rotatef(drot, 0, 0, 1);
            break;
        case SDLK_i:
            //vldr.setPos(vldr.getPos() + Vector3D(0, dtrl, 0));
            vldr.rotatef(drot, 1, 0, 0);
            break;
        case SDLK_k:
            //vldr.setPos(vldr.getPos() + Vector3D(0, -dtrl, 0));
            vldr.rotatef(-drot, 1, 0, 0);
            break;
        case SDLK_l:
            vldr.rotY(-drot);
            break;
        case SDLK_j:
            vldr.rotY(drot);
            break;
        case SDLK_e:
            vldr.setPos(vldr.getPos() + vldr.getDir()*dtrl);
            break;
        case SDLK_q:
            vldr.setPos(vldr.getPos() - vldr.getDir()*dtrl);
            break;
        case SDLK_w:
            vldr.setPos(vldr.getPos() + vldr.getUp()*dtrl);
            break;
        case SDLK_s:
            vldr.setPos(vldr.getPos() - vldr.getUp()*dtrl);
            break;
        case SDLK_d:
            vldr.setPos(vldr.getPos() - vldr.getX()*dtrl);
            break;
        case SDLK_a:
            vldr.setPos(vldr.getPos() + vldr.getX()*dtrl);
            break;
        case SDLK_t:
            ancla.y += dtrl;
            break;
        case SDLK_g:
            ancla.y -= dtrl;
            break;
        case SDLK_h:
            ancla.x += dtrl;
            break;
        case SDLK_f:
            ancla.x -= dtrl;
            break;
        case SDLK_r:
            ancla.z += dtrl;
            break;
        case SDLK_y:
            ancla.z -= dtrl;
            break;
    }
    if (anclar
        && (p.sym == SDLK_t
            || p.sym == SDLK_g
            || p.sym == SDLK_f
            || p.sym == SDLK_h
            || p.sym == SDLK_r
            || p.sym == SDLK_y)) {
        if(scosm) {
            for (SistemaCubos &cube : cubes) {
                cube.setPos(0, ancla);
            }
        } else {
            sm.setPos(0, ancla);
        }
    }

}

void Gestor::onKeyPressed(SDL_Event &e)
{
    int entero;

    if (e.type == SDL_KEYUP)
        return;

    switch(e.key.keysym.sym)
    {
        case SDLK_SPACE:
            semaforoDisplay.sumar();
            semaforoStep.sumar();
            break;
        case SDLK_p:	// play / stop
            if (semaforoStep.isOpen()) {
                semaforoStep.cerrar();
                semaforoDisplay.abrir();    // reverse, so that the semaforoStep controls when to open semaforoDisplay
                //semaforoDisplay.Cerrar();
                //semaforoDisplay.Sumar();
            } else {
                semaforoStep.abrir();
                semaforoDisplay.cerrar();
                //semaforoDisplay.Abrir();
            }
            break;
            //case SDLK_h:
            ////SDL_LOG();
            //cout << "Ayuda: asdf\n\t"
            //"r: redraw\n\t"
            //"c: cuadros por segundo (FPS)\n\t"
            //;	// TODO print_file("ayuda.txt");
            //break;
            //case SDLK_r:	// redraw
            //semaforoDisplay.Sumar();
            //break;
        case SDLK_c:
            cout << "Nuevos FPS: " << endl;
            SDLcin(entero);
            setFps(entero);
            break;
        case SDLK_v:
            cout << "Nueva velocidad (" << velocidad <<"): " << endl;
            SDLcin(entero);
            velocidad = entero;
            break;
        case SDLK_x:
            anclar = !anclar;
            if (anclar) {
                if (scosm) {
                    if (cubes.size() > 0 ) {
                        ancla = cubes[0].getPos(0);
                    }
                } else {
                    ancla = sm.getPos(0);
                }
            }
            break;
        case SDLK_F1:
        case SDLK_QUESTION:
            printHelp();
            break;
        default:
            //cout << " tecla inutil pulsada: ";
            //cout << "e.key.keysym.sym = " << e.key.keysym.sym << endl;	// DEPURACION
            //cout << "e.key.keysym.scancode = " << e.key.keysym.scancode << endl;	// DEPURACION
            //cout << "e.key.keysym.mod = " << e.key.keysym.mod << endl;	// DEPURACION
            break;
    }
}

void Gestor::onMouseButton(SDL_Event &e)
{
    last_click_x = e.button.x;
    last_click_y = e.button.y;

    semaforoDisplay.sumar();
}

void Gestor::onMouseMotion(SDL_Event &e)
{
    xant = e.button.x;
    yant = e.button.y;

    semaforoDisplay.sumar();
}

void Gestor::onEvent(SDL_Event &e) {

    if (controlEvent == e.type) {
        switch ((Command)e.user.code) {
            case Command::QUIT:
                semaforoDisplay.cerrar();
                semaforoStep.cerrar();
//                pool.sendExitSignal();
                salir();
                break;
            case Command::PAUSE:
                semaforoStep.cerrar();
                semaforoDisplay.abrir();    // reverse, so that semaforoStep controls semaforoDisplay
                break;
            case Command::FREEZE:
                semaforoStep.cerrar();
                semaforoDisplay.cerrar();
                break;
            case Command::RESUME:
                semaforoStep.abrir();
                semaforoDisplay.cerrar();    // reverse, so that semaforoStep controls semaforoDisplay
                break;
            case Command::ADD:
                addCubome(string((char *) e.user.data1));
                break;
            case Command::VERBOSITY:
                setLogLevel(string((char *) e.user.data1));
                break;
            default:
                SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION, "unknown event code: %d\n", e.user.data1);
                break;
        }
    }
}

void Gestor::printHelp() {
    stringstream ss;
    ss << "** simulator window help ** You can move around with wasdqe and ijkluo.\n"
    << " w: move up\n"
    << " a: move left\n"
    << " s: move down\n"
    << " d: move right\n"
    << " q: move back (mnemonic: quit)\n"
    << " e: move forward (mnemonic: enter)\n\n"
    << " i: turn down\n"
    << " j: turn left\n"
    << " k: turn up\n"
    << " l: turn right\n"
    << " u: roll left\n"
    << " o: roll right\n\n"
    << " SPACE: while paused, do only one iteration.\n\n"
    << " p: play/pause simulation. You can still move.\n"
    << " c <number>: change computation FPS.\n"
    << " v <number>: change velocity/precision. 10 is precise, 2 is fast, and below is unstable.\n"
    << " x: capture the first node (only in play mode). It can be moved with tfghry the same way that wasdqe.\n\n";
    cout << ss.str();
}

void Gestor::setControlEvent(Uint32 controlEvent) {
    this->controlEvent = controlEvent;
    SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "controlEvent set in gestor to: %d\n", controlEvent);
}

void Gestor::setThreads(int threads) {
    if (threads > 0) {
        this->simulationThreads = (Uint32) threads;
    }
}

void Gestor::addCubome(const string &cbmPath) {
    SDL_LogInfo(SDL_LOG_CATEGORY_APPLICATION, "before: cubes.size(): %d, ", cubes.size());
    cubes.resize(cubes.size() + 1);
    cubes.back().load(cbmPath);
    cubes.back().setAcelGlobal({0, -10, 0});
    SDL_LogInfo(SDL_LOG_CATEGORY_APPLICATION, "after: cubes.size(): %d\n", cubes.size());
}

void Gestor::setLogLevel(string level) {
    if (level == "debug") {
        SDL_LogSetPriority(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_DEBUG);
    } else if (level == "info") {
        SDL_LogSetPriority(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_INFO);
    } else if (level == "error") {
        SDL_LogSetPriority(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_ERROR);
    }
    SDL_LogInfo(SDL_LOG_CATEGORY_APPLICATION, "changing gestor log level to %d",
            SDL_LogGetPriority(SDL_LOG_CATEGORY_APPLICATION));
}
