#ifndef _MEMORIA_H_
#define _MEMORIA_H_

#include <SDL.h>
#include <complex>
#include "threads/Pool.h"
#include "math/Vector3D.h"
#include "graphics/drawables/Volador.h"
#include "SistemaMuelles.h"
#include "SistemaCubos.h"

struct Memoria
{
    int last_click_x, last_click_y;
    int xant, yant;
    int t;
    Volador vldr;
    SistemaMuelles sm;
    vector<SistemaCubos> cubes;
    float drot, dtrl, dmv;
    Vector3D ancla;
    bool anclar;
    float velocidad;
    int nCubos;
    bool scosm;
};


#endif
