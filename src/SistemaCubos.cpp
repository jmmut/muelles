#include "SistemaCubos.h"


SistemaCubos::SistemaCubos() {
    x = y = z = yx = 0;
}

SistemaCubos::~SistemaCubos() {
}

void SistemaCubos::build (unsigned int xCubes, unsigned int yCubes, unsigned int zCubes, float lado) {
    unsigned int ix, iy, iz;
    x = xCubes;
    y = yCubes;
    z = zCubes;
    yx = yCubes*xCubes;
    zyx = z*yx;
    xNodes = xCubes +1;
    yNodes = yCubes +1;
    zNodes = zCubes +1;
    yxNodes = yNodes*xNodes;

    //unsigned int xaux, yaux, zaux;

    for (iz = 0; iz < zNodes; iz++) {
        for (iy = 0; iy < yNodes; iy++) {
            for (ix = 0; ix < xNodes; ix++) {
                g.addNamedNode(Node(Vector3D(ix, iy, iz)*lado), hashNode(ix, iy, iz));
            }
        }
    }

    for (iz = 0; iz < z; iz++) {
        for (iy = 0; iy < y; iy++) {
            for (ix = 0; ix < x; ix++) {
                unsigned int nodes[8] = {
                        hashNode(ix, iy, iz),
                        hashNode(ix+1, iy, iz),
                        hashNode(ix, iy+1, iz),
                        hashNode(ix, iy, iz+1),
                        hashNode(ix+1, iy+1, iz),
                        hashNode(ix, iy+1, iz+1),
                        hashNode(ix+1, iy, iz+1),
                        hashNode(ix+1, iy+1, iz+1)
                };
                setCuboTensegrity(lado, nodes);
            }
        }
    }
    Cubo c;
    cubes.clear();
    cubes.reserve(zyx);
    for (unsigned int i = 0; i < zyx; i++) {
        c.tipo = CubeType(1);
        cubes.push_back(c);
    }
}

void SistemaCubos::load(Cubo *cubesP, unsigned int cuantos[3], float lado) {
    x = cuantos[0];
    y = cuantos[1];
    z = cuantos[2];
    yx = y*x;
    zyx = z*yx;
    xNodes = x +1;
    yNodes = y +1;
    zNodes = z +1;
    yxNodes = yNodes * xNodes;

    unsigned int ix, iy, iz;
    for (iz = 0; iz < zNodes; iz++) {
        for (iy = 0; iy < yNodes; iy++) {
            for (ix = 0; ix < xNodes; ix++) {
                g.addNamedNode(Node(Vector3D(ix, iy, iz)*lado), hashNode(ix, iy, iz));
            }
        }
    }

    CubeType ct;
    for (iz = 0; iz < z; iz++) {
        for (iy = 0; iy < y; iy++) {
            for (ix = 0; ix < x; ix++) {
                unsigned int nodes[8] = {
                        hashNode(ix, iy, iz),
                        hashNode(ix+1, iy, iz),
                        hashNode(ix, iy+1, iz),
                        hashNode(ix, iy, iz+1),
                        hashNode(ix+1, iy+1, iz),
                        hashNode(ix, iy+1, iz+1),
                        hashNode(ix+1, iy, iz+1),
                        hashNode(ix+1, iy+1, iz+1)
                };
                ct = cubesP[hashCube(ix, iy, iz)].tipo;
                if (ct != EMPTY) {
                    //setCuboTensegrity(lado, nodes);
                    setCubo(lado, nodes, ct);
                }
            }
        }
    }

    //for (unsigned int i = 0; i < yx*z; i++) {
    //cubes[i] = cubesP[i];
    //}
    cubes.clear();
    cubes.reserve(zyx);

    for (unsigned int i = 0; i < zyx; i++) {
        cubes.push_back(cubesP[i]);
    }
    //cubes.push_back(cubesP);
    //memcpy(cubes.data(), cubesP, zyx);

    // additional edges
    /*
       float diag3 = (x+y+z)*2*lado;
       g.addEdge(hashNode(xNodes-1, yNodes-1, zNodes-1), hashNode(0, 0, 0), {diag3, diag3} );
       g.addEdge(hashNode(0, yNodes-1, zNodes-1), hashNode(xNodes-1, 0, 0), {diag3, diag3} );
       g.addEdge(hashNode(xNodes-1, 0, zNodes-1), hashNode(0, yNodes-1, 0), {diag3, diag3} );
       g.addEdge(hashNode(xNodes-1, yNodes-1, 0), hashNode(0, 0, zNodes-1), {diag3, diag3} );
       */
}

void SistemaCubos::load(istream &in, unsigned int cuantos[3], float lado) {
    int nCubos = cuantos[0]*cuantos[1]*cuantos[2];
    Cubo *c = new Cubo[nCubos];
    int tipo;
    for (int i = 0; i < nCubos && in; i++) {
        in >> tipo;
        c[i].tipo = CubeType(tipo);
    }
    load(c, cuantos, lado);
    delete[] c;
}

void SistemaCubos::load(const string &cubomaPath) {
    ifstream f;
    f.open(cubomaPath.c_str());
    if (!f) {
        SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION, "file %s couldn't be opened to load cuboma\n", cubomaPath.c_str());
    } else {
        this->cbmPath = cubomaPath;
        unsigned int cuantos[3];
        f >> cuantos[0];
        f >> cuantos[1];
        f >> cuantos[2];
        load(f, cuantos, 0.5);
    }
}


void SistemaCubos::setupSimulation(const string &cbmPath, const string &logPath){
    this->cbmPath = cbmPath;	//innecesario
    this->logPath = logPath;
    load(cbmPath);
}
/** @return 1: simulation finished and writed. @n 0: running simulation. @n -1: fatal error, 
 * cube graph is not well-formed.@n 2: minor error, file could not be opened.*/
int SistemaCubos::runSimulation (float finishTime, float dt, float &distance) {
    static float time = 0;
    if (cubes.empty()) {
        return -1;
    }

    time += dt;

    avanzar(dt);

    Node n;
    g.getNode(0, n);

    //distance = sqrt(n.pos[actual].x*n.pos[actual].x + n.pos[actual].z*n.pos[actual].z);
    distance = 0;
    g.forEachNode([&](Node &n){
        distance += sqrt(n.pos[actual].x*n.pos[actual].x + n.pos[actual].z*n.pos[actual].z);
    });

    distance /= g.getNodesCount();

    if (time >= finishTime) {
        writeSimulation(distance);
        return 1;
    }
    return 0;
}

void SistemaCubos::writeSimulation (float dist) {
    cout << "distancia recorrida: " << dist << endl;

    if (!logPath.empty()) {
        ofstream f;

        f.open(logPath.c_str());
        if (!f) {
            SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "file %s could not be opened\n", logPath.c_str());
        }

        f << dist << endl;
    }
}
unsigned int SistemaCubos::hashCube(unsigned int ix, unsigned int iy, unsigned int iz) {
    return  iz*yx + iy*x + ix;
}
unsigned int SistemaCubos::hashNode(unsigned int ix, unsigned int iy, unsigned int iz) {
    return  iz*yxNodes + iy*xNodes + ix;
}
void SistemaCubos::unhashCube(unsigned int n, unsigned int &xCube, unsigned int &yCube, unsigned int &zCube) {
    xCube = n % x;
    yCube = (n % yx )/ x;
    zCube = n / yx;
}
void SistemaCubos::unhashNode(unsigned int n, unsigned int &xNode, unsigned int &yNode, unsigned int &zNode) {
    xNode = n % xNodes;
    yNode = (n % yxNodes )/ xNodes;
    zNode = n / yxNodes;
}
unsigned int SistemaCubos::nodeToCube(unsigned int node) {
    unsigned int x, y, z;
    unhashNode(node, x, y, z);
    return hashCube(x, y, z);
}

void SistemaCubos::getColor(unsigned int cube, float color[4]) {
    if (cube >= zyx) {
        color[0] = 1.0;
        color[1] = 1.0;
        color[2] = 1.0;
        color[3] = 0.2;
        return;
    }

    switch (cubes[cube].tipo) {
        case EMPTY:
        case STATIC:
            color[0] = 0.0;
            color[1] = 0.0;
            color[2] = 1.0;
            color[3] = 0.2;
            break;
        case EXPAND_CONTRACT:
            color[0] = 1.0;
            color[1] = 0.0;
            color[2] = 0.0;
            color[3] = 0.2;
            break;
        case CONTRACT_EXPAND:
            color[0] = 0.0;
            color[1] = 1.0;
            color[2] = 0.0;
            color[3] = 0.2;
            break;
        default:
            color[0] = 1.0;
            color[1] = 1.0;
            color[2] = 0.0;
            color[3] = 0.2;
            break;
    }
}

void SistemaCubos::changeLong(float dt) {
    float variationAmplitude = 0.2;
    float cachedSin = sin(M_PI*t)*variationAmplitude;

    g.forEachEdge([&](Muelle &m){
        switch(m.tipo)
        {
            case EXPAND_CONTRACT:
                m.longitudActual = m.longitudReposo + m.longitudReposo*cachedSin;
                break;
            case CONTRACT_EXPAND:
                m.longitudActual = m.longitudReposo - m.longitudReposo*cachedSin;
                break;
            case STATIC:
                break;
            default:
                break;
        }
    });
}
/*
   void SistemaCubos::changeLong(unsigned int cubeId, float dt) {
   if (cubeId < zyx) {
   unsigned int x, y, z;
   float variationAmplitude = 0.2;
   float cachedSin = sin(4*t)*variationAmplitude;
//float cachedSin2 = sin(6*t)*variationAmplitude;
unsigned int ngs[8];
unhashCube(cubeId, x, y, z);
Edge *e1;//, *e2;
ngs[0] = hashNode(x, y, z);
ngs[1] = hashNode(x+1, y, z);
ngs[2] = hashNode(x, y+1, z);
ngs[3] = hashNode(x, y, z+1);
ngs[4] = hashNode(x+1, y+1, z);
ngs[5] = hashNode(x, y+1, z+1);
ngs[6] = hashNode(x+1, y, z+1);
ngs[7] = hashNode(x+1, y+1, z+1);
//for (tuple<unsigned int, unsigned int, int> p: getCubeEdges())
//g.getEdgeValue(ngs[get<0>(p)], ngs[get<1>(p)], e1);
for (unsigned int i = 0; i < cubeEdges.length; i++) {
if (g.getEdgeValue(ngs[cubeEdges.cubeEdge[i].src], ngs[cubeEdges.cubeEdge[i].dst], e1)) {
switch (cubes[cubeId].tipo)
{
case EXPAND_CONTRACT:
e1->longitudActual = e1->longitudReposo + e1->longitudReposo*cachedSin; 
break;
case CONTRACT_EXPAND:
e1->longitudActual = e1->longitudReposo - e1->longitudReposo*cachedSin; 
break;
case STATIC:
break;
default:
cout << "como que cubo default??" << cubeId << " -> " << nodeToCube(cubeId) << ": " << cubes[nodeToCube(cubeId)].tipo << endl;
break;
}
g.replaceEdge(ngs[cubeEdges.cubeEdge[i].src], ngs[cubeEdges.cubeEdge[i].dst], *e1);
}
}
}
}*/

/*
   void SistemaCubos::draw() {
   unsigned int i;
   Neighbours *ng;
   Node nodo, vecino;
   float color[4];

   glBegin(GL_LINES);
   for (i = 0; i < g.getNodesCount(); i++) {	// para cada nodo
   if (g.getNeighbours(i, ng)) {
   g.getNode(i, nodo);
   getColor(nodeToCube(i), color);
   glColor4fv(color);
   for (const auto &e: *ng) {	// y para cada uno de sus vecinos
   g.getNode(e.first, vecino);
   glVertex3fv(nodo.pos[actual].v);	// pintamos una linea
   glVertex3fv(vecino.pos[actual].v);	//
   }
   }
   }
   glEnd();
   }
   */


//SistemaCubos::
