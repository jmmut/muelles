#ifndef _SISTEMA_MUELLES_
#define _SISTEMA_MUELLES_

#include <tuple>
#include "math/Vector3D.h"
#include "utils/graph/Undirgravec.h"
#include "vgm/Ventana.h"

struct CubeEdges;
enum CubeType {
    EMPTY = 0,
    STATIC = 1,
    EXPAND_CONTRACT = 2,
    CONTRACT_EXPAND = 3
};

struct Cubo {
    CubeType tipo;

    //bool operator==(const Cubo &c2) const {
    //return (first == other.first
    //&& second == other.second
    //&& third == other.third);
    //}
};
class SistemaMuelles {
protected:
public:
    struct Muelle {
        float longitudReposo;
        float longitudActual;
        CubeType tipo;
        friend ostream& operator<< (ostream &o, const Muelle &m) {return o << m.longitudReposo << ", " << m.longitudActual;};
    };
    struct Node {
        Vector3D pos[2];
        Vector3D vel[2];
        Vector3D fuerza;
        Vector3D vecinos;
        //Vector3D sigPos;	// you shouldn't compute next position A with another already updated postion B
        //Vector3D sigVel;
        Node(){};
        Node(Vector3D ini):pos{ini,Vector3D(0, 0, 0)},vel{Vector3D(0, 0, 0), Vector3D(0, 0, 0)}{};
        friend ostream& operator<< (ostream& o, const Node &n) {return o << n.pos[0];};
    };
    typedef Undirgravec<Node, Muelle> Grafo;
//    typedef Grafo::Neighbours Neighbours;
    //typedef Grafo::Edge Edge;
    static constexpr float k = 50;	// constante del muelle : N/m
    static constexpr float r = 0.3;//0.3;	// rozamiento: perdida de fuerza. unidades: s*N/m
    static constexpr float m = 0.04;	// masa de cada nodo
    static const CubeEdges cubeEdges;

    Vector3D grav;	// aceleracion global (como la gravedad o el viento)
    Grafo g;
    double t;
    bool actual;

public:
    SistemaMuelles();
    ~SistemaMuelles();
    unsigned int getNodesCount();
    void avanzar(float dt);
    virtual void changeLong(float dt);
    //virtual void changeLong(unsigned int id, float dt);
    void anclar(unsigned int nodo);	// deshacer el cambio de posicion y velocidad de la ultima llamada a avanzar
    void setPos(unsigned int nodo, Vector3D posicion);
    Vector3D getPos(unsigned int nodo);
    void setCuerda(int numNodes, float longitud);
    void setCubo(float lado, CubeType tipo);
    void setCubo(float lado, unsigned int nodes[8], CubeType tipo);
    void setTetra(float lado);
    void appendCubo(Vector3D dir, unsigned int cuantos, unsigned int nodo[4]);
    void appendCubo2(unsigned int nodo[6]);
    void appendCubo3(unsigned int nodo[7]);
    void setTriangTensegrity(float lado);
    void setTetraTensegrity(float lado);
    void setCuboTensegrity(float lado);
    void setCuboTensegrity(float lado, unsigned int ids[8]);
    void setMalla3D(unsigned int nX,unsigned int nY,unsigned int nz, float lado);
    //vector<tuple<unsigned int, unsigned int, int>> getCubeEdges();
    CubeEdges getCubeEdges();
    void setAcelGlobal(Vector3D a);
    void getColor (CubeType type, float color[4]);
    virtual void draw();
    void print(ostream &o);
private:
    Vector3D getSumaVelocidadVecinos(unsigned int nodo);
    Vector3D getSumaFuerzaVecinos(unsigned int nodo);
};

struct CubeEdge {
    unsigned int src, dst;
    int type;
};
struct CubeEdges {
    const static unsigned int length {28};
    CubeEdge cubeEdge[length] {
            {0, 1, 1},
            {0, 2, 1},
            {0, 3, 1},
            {4, 1, 1},
            {4, 2, 1},
            {4, 7, 1},
            {5, 2, 1},
            {5, 3, 1},
            {5, 7, 1},
            {6, 1, 1},
            {6, 3, 1},
            {6, 7, 1},
            {0, 7, 3},
            {1, 5, 3},
            {6, 2, 3},
            {3, 4, 3},
            {0, 4, 2},
            {0, 5, 2},
            {0, 6, 2},
            {6, 4, 2},
            {4, 5, 2},
            {5, 6, 2},
            {7, 1, 2},
            {7, 2, 2},
            {7, 3, 2},
            {1, 2, 2},
            {2, 3, 2},
            {3, 1, 2}
    };
};
#endif // _SISTEMA_MUELLES_
