#!/bin/bash
#
# -first you must decompress, if you didn't already:
#tar -zxvf release.tar.gz
#
# -next enter into the directory
#cd release*
#

# get randomize/common-libs dependencies
mkdir -p randomize
cd randomize
wget https://bitbucket.org/mutcoll/randomize/get/master.zip
unzip master.zip
cd mut*/common-libs
make
sudo make install
cd ../../..


# you can build the program with make, 
# the file is in the src directory
cd src
make
cd ..

# finally, to launch:
#./muelles
