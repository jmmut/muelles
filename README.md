# muelles

## Simulation of springs.

This project was first thought to simulate unidimensional strings, but later it was improved to support any graph-able structure.

## Quick Installing

Currently, the easiest way to compile this is installing [biicode](https://www.biicode.com/downloads) and doing the next commands:


```
bii init muelles -l clion
cd muelles
bii setup:cpp
bii open randomize/muelles
bii configure -DCMAKE_BUILD_TYPE=RELWITHDEBINFO
bii build
cd bin
./randomize_muelles_src_main -h
```

That will hopefully give you a working binary. In the process, you will compile the SDL, it's easier this way.

## Step by step installing

If the previous commands didn't work for you, here there are explanations of what the idea was, so you can find out the way that works for you.

* `bii init muelles -l clion`

This creates a folder named `muelles` and initializes a bii project inside it, with the bonus of being able to open it with [clion](https://www.jetbrains.com/clion/). This step requires you have [downloaded and installed biicode](https://www.biicode.com/downloads).


* `bii setup:cpp`

To compile the project you will need gcc, g++ and cmake. If you don't have those in the version that biicode wants, this command will install them for its use in biicode.


* `bii open randomize/muelles`

This is the equivalent of `git checkout` in biicode. Basically, downloads a copy of the project, and prepares its compilation.

* `bii configure -DCMAKE_BUILD_TYPE=RELWITHDEBINFO`

This project is somewhat CPU intensive, so it is quite important to use optimized compilation. Some tests point out that the speed is ~2x.

* `bii build`

The actual compilation command. Unfortunately, for the moment the easiest way is to compile the SDL and all the other sources at this step. Yes that means that if you install this in 10 computers, you will compile the SDL 10 times :/

## Running

To launch the program:
```
./randomize_muelles_src_main <cubome.cbm>
```
The <cubome.cbm> is a path **relative to your current path**.

Also, to see a default cubome:
```
./randomize_muelles_src_main
```
Passing the argument `-h` in the command line will display full help.

Pressing `F1` in the window will display the controls in the terminal.

One specific structure is a x\*y\*z cubes mesh.

It can load a cubome (cube genome) which specifies the pattern of types the mesh is composed.

The cubome shows the size of the cube, and the type of every cube. The order of read of the dimensions is: x, y, z, in ascendent order. Example cubome:

```
3 2 4

1 1 1
1 1 1

2 2 2
2 2 2

2 3 3
2 3 3

3 3 3
3 3 3
```
